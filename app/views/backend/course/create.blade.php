@extends('backend/layout/master')

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Course
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {{Form::open(['route' => 'courses.store'])}}

                                <div class="form-group">
                                    {{ Form::label('Select Branch') }}
                                    {{ Form::select('branch_id', $branch, null, ['class' => 'form-control']) }}
                                    <span class="alert-danger">{{ $errors->first('branch_id') }}</span>
                                </div>

                                <div class="form-group">
                                    {{ Form::label('Title') }}
                                    {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter Title', 'autofocus' => 'autofocus']) }}
                                    <span class="alert-danger">{{ $errors->first('title') }}</span>
                                </div>
                                
                                <div class="form-group">
                                    {{ Form::label('Description') }}
                                    {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Enter Description']) }}
                                    <span class="alert-danger">{{ $errors->first('description') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::submit('Create', ['class' => 'form-control btn btn-primary']) }}
                                </div>
                            {{ Form::close() }}
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
            <!-- /.col-lg-12 -->
        </div>
            <!-- /.row -->
@stop