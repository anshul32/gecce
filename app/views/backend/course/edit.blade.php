@extends('backend/layout/master')

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Course
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {{ Form::model($course,['method' => 'PATCH', 'route'=>['courses.update',$course['id'] ]]) }}
                            	<input type="hidden" name="_method" value="PUT" />
                                <div class="form-group">
                                    {{ Form::label('Title') }}
                                    {{ Form::text('title', $course['title'], ['class' => 'form-control', 'placeholder' => 'Enter Title', 'autofocus' => 'autofocus']) }}
                                    <span class="alert-danger">{{ $errors->first('title') }}</span>
                                </div>
                                
                                <div class="form-group">
                                    {{ Form::label('Description') }}
                                    {{ Form::textarea('description', $course['description'], ['class' => 'form-control', 'placeholder' => 'Enter Description']) }}
                                    <span class="alert-danger">{{ $errors->first('description') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::submit('Update', ['class' => 'form-control btn btn-primary']) }}
                                </div>
                            {{ Form::close() }}
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
            <!-- /.col-lg-12 -->
        </div>
            <!-- /.row -->
@stop