@extends('backend/layout/master')

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Subject
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {{Form::open(['route' => 'subjects.store'])}}
                                <div class="form-group">
                                    {{ Form::label('Syllabus') }}
                                    {{ Form::select('syllabus_id', $syllabus, null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}
                                    <span class="alert-danger">{{ $errors->first('syllabus_id') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('Title') }}
                                    {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter Title']) }}
                                    <span class="alert-danger">{{ $errors->first('title') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('Subject Type') }}
                                    {{ Form::select('type', [ 1 => 'Theory', 2 => 'Practical' ], null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}
                                    <span class="alert-danger">{{ $errors->first('type') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('Marks') }}
                                    {{ Form::number('marks', null, ['class' => 'form-control', 'placeholder' => 'Enter Marks']) }}
                                    <span class="alert-danger">{{ $errors->first('marks') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::submit('Create', ['class' => 'form-control btn btn-primary']) }}
                                </div>
                            {{ Form::close() }}
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
            <!-- /.col-lg-12 -->
        </div>
            <!-- /.row -->
@stop