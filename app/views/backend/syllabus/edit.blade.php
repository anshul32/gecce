@extends('backend/layout/master')

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Syllabus
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {{-- <form role="form"> --}}
                            {{Form::open(['route' => 'syllabus.store'])}}
                            	<div class="form-group">
                                    {{ Form::label('Course') }}
                                    {{ Form::select('course_id', $courses, $syllabus['course']['id'], ['class' => 'form-control', 'autofocus' => 'autofocus']) }}
                                    <span class="alert-danger">{{ $errors->first('course_id') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('Title') }}
                                    {{ Form::text('title', $syllabus['title'], ['class' => 'form-control', 'placeholder' => 'Enter Title']) }}
                                    <span class="alert-danger">{{ $errors->first('title') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::submit('Create', ['class' => 'form-control btn btn-primary']) }}
                                </div>
                            {{-- </form> --}}
                            {{ Form::close() }}
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
            <!-- /.col-lg-12 -->
        </div>
            <!-- /.row -->
@stop