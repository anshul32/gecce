@extends('backend/layout/master')

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Course
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {{-- <form role="form"> --}}
                            {{Form::open(['route' => 'downloads.store'])}}
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" placeholder="Enter Title" name="title" id="title">
                                    <span class="alert-danger">{{ $errors->first('title') }}</span>
                                </div>
                                <div class="form-group">
                                    <label>Download Url</label>
                                    <input type="text" class="form-control" placeholder="Enter Download Url" name="url" id="url">
                                    <span class="alert-danger">{{ $errors->first('url') }}</span>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Create" class="form-control btn btn-primary">
                                </div>
                            {{-- </form> --}}
                            {{ Form::close() }}
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
            <!-- /.col-lg-12 -->
        </div>
            <!-- /.row -->
@stop