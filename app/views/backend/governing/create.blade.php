@extends('backend/layout/master')

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Course
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {{-- <form role="form"> --}}
                            {{Form::open(['route' => 'governings.store'])}}
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" placeholder="Enter name" name="name" id="name">
                                    <span class="alert-danger">{{ $errors->first('name') }}</span>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description" id="description" placeholder="description"></textarea>
                                    <span class="alert-danger">{{ $errors->first('description') }}</span>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Create" class="form-control btn btn-primary">
                                </div>
                            {{-- </form> --}}
                            {{ Form::close() }}
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
            <!-- /.col-lg-12 -->
        </div>
            <!-- /.row -->
@stop