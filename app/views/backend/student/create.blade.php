@extends('backend/layout/master')

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Student
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {{Form::open(['route' => 'students.store'])}}
                                <div class="form-group">
                                    {{ Form::label('Name') }}
                                    {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter name']) }}
                                    <span class="alert-danger">{{ $errors->first('name') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('Date Of Birth') }}
                                    {{ Form::text('dob', null, ['class' => 'form-control', 'placeholder' => 'Enter Date  Of Birth']) }}
                                    <span class="alert-danger">{{ $errors->first('dob') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('Place Of Birth') }}
                                    {{ Form::text('place_of_birth', null, ['class' => 'form-control', 'placeholder' => 'Enter Place Of Birth']) }}
                                    <span class="alert-danger">{{ $errors->first('place_of_birth') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('Fathers Name') }}
                                    {{ Form::text('father_name', null, ['class' => 'form-control', 'placeholder' => 'Enter Fathers Name']) }}
                                    <span class="alert-danger">{{ $errors->first('father_name') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('Mothers Name') }}
                                    {{ Form::text('mother_name', null, ['class' => 'form-control', 'placeholder' => 'Enter Mothers Name']) }}
                                     <span class="alert-danger">{{ $errors->first('mother_name') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('Address') }}
                                    {{ Form::textarea('address', null, ['class' => 'form-control', 'placeholder' => 'Enter Address']) }}
                                    <span class="alert-danger">{{ $errors->first('address') }}</span>
                                </div>
                                <div class="form-group">
                                	{{ Form::label('Contact No.') }}
                                    {{ Form::number('contact', null, ['class' => 'form-control', 'placeholder' => 'Enter Contact No']) }}
                                    <span class="alert-danger">{{ $errors->first('contact') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('Language') }}
                                    {{ Form::text('language', null, ['class' => 'form-control', 'placeholder' => 'Enter Language']) }}
                                    <span class="alert-danger">{{ $errors->first('language') }}</span>
                                </div>
                                <div class="form-group">
	                                <label>Gender</label>
	                                <div class="radio">
	                                    <label>
	                                        <input type="radio" name="gender" id="male" value="Male" checked>Male
	                                    </label>
	                                </div>
	                                <div class="radio">
	                                    <label>
	                                        <input type="radio" name="gender" id="female" value="option2">Female
	                                    </label>
	                                </div>
	                            </div>
	                            <div class="form-group">
                                    {{ Form::label('Nationality') }}
                                    {{ Form::text('nationality', null, ['class' => 'form-control', 'placeholder' => 'Enter Nationality']) }}
                                    <span class="alert-danger">{{ $errors->first('nationality') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('Select Course') }}
                                    {{ Form::select('course_id', $course, null, ['class' => 'form-control']) }}
	                            </div>
	                            <div class="form-group">
                                {{ Form::label('Qualification') }}
                                    {{ Form::text('qualification', null, ['class' => 'form-control', 'placeholder' => 'Enter Your Qualification']) }}
                                    <span class="alert-danger">{{ $errors->first('qualification') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('Select Branch') }}
                                    {{ Form::select('branch_id', $branch, null, ['class' => 'form-control']) }}
	                            </div>
	                            <div class="form-group">
                                    {{ Form::label('About') }}
                                    {{ Form::text('about', null, ['class' => 'form-control', 'placeholder' => 'About You']) }}
                                    <span class="alert-danger">{{ $errors->first('about') }}</span>
                                </div>
                                <div class="form-group">
                                    {{ Form::submit('Create', ['class' => 'form-control btn btn-primary']) }}
                                </div>
                            {{ Form::close() }}
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
                <!-- /.col-lg-12 -->
    </div>
            <!-- /.row -->
@stop