@extends('backend/layout/master')

@section('content')
    <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit branch
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                {{Form::open(['route' => 'branchs.store'])}}
                                    <div class="form-group">
                                        {{ Form::label('Manager Name') }}
                                        {{ Form::text('manager_name', $branch['manager_name'], ['class' => 'form-control', 'placeholder' => 'Enter Manager Name', 'autofocus' => 'autofocus']) }}
                                        <span class="alert-danger">{{ $errors->first('manager_name') }}</span>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('Email') }}
                                        {{ Form::email('email', $branch['email'], ['class' => 'form-control', 'placeholder' => 'Enter Email']) }}
                                        <span class="alert-danger">{{ $errors->first('email') }}</span>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('Contact No.') }}
                                        {{ Form::number('contact', $branch['contact'], ['class' => 'form-control', 'placeholder' => 'Enter Contact No.']) }}
                                        <span class="alert-danger">{{ $errors->first('contact') }}</span>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('Address') }}
                                        {{ Form::textarea('address', $branch['address'], ['class' => 'form-control', 'placeholder' => 'Enter Address']) }}
                                        <span class="alert-danger">{{ $errors->first('address') }}</span>
                                    </div>
                                    <div class="form-group">
                                        {{ Form::submit('Update', ['class' => 'form-control btn btn-primary']) }}
                                    </div>
                                {{ Form::close() }}
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
@stop