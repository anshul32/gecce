@extends('backend/layout/master')

@section('content')
	<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Branch lists
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Manager Name</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                            <th>Address</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($branchs as $b )
                                                                             
                                        <tr class="odd gradeX">
                                            <td>{{ $b['manager_name'] }}</td>
                                            <td>{{ $b['email'] }}</td>
                                            <td>{{ $b['contact'] }}</td>
                                            <td>{{ $b['address'] }}</td>
                                            <td>{{ link_to_route('branchs.edit', 'Edit', $b['id']) }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
@stop