@extends('frontend/layout/master')
  @section('content')
  	<div id="contentBk" class="clearfix">
      <div id="content">
          <div class="topImg clearfix">
              {{HTML::image('images/headers/header_2.jpg', 'About Us')}}
              <p>Our <strong>Course</strong></p>
          </div>
        <div class="wrapper">
            <div class="course column c-67 clearfix">
              
              <div class="box">
                <h4>Nursery Teacher Traning :</h4>
                <div class="boxInfo examInfo">
                  <div>
                    {{HTML::image('images/course/img2.jpg', '')}}
                    <p style="text-align:justify;">According to Indian Govt. new education policy that it is necessary to appoint trained teacher in every school. In facts the teacher have good knowledge. Highly qualified trained teacher have not good knowledge about childhood. They unable to understand the various behavioral problem of little children. Modern time nursery school are the root of basic education. So N.T.T. is necessary in present time. So get better jobs in pre-primary education field.<br><br>
 
                    Many students seek admissions in B.Ed. after graduation. Aspirants of B.Ed programmed are unaware of the fact of job opportunities. About 7% vacancies come up are B.Ed. as compared to 65% for N.T.T.One may join N.T.T. even after M.A. for job opportunities. The untrained in-service teachers from various schools are also joining this course as these days because N.T.T. has become essential pre-requisite for teacher of nursery classes.<br><br>
                     
                    The national policy of education adopted in 1986 by Govt. of INDIA recognized the need of early childhood care, education and emphasized the need of all round development of child during the critical early and moral. The present nursery teacher training programmed is specially meet to develop these abilities in them.<br><br>
                     
                    The N.T.T. Programmed is a well recognized course to train and promote the education girls and there after they can be able to choose teaching as their carrier. This is to become reputed teacher in reputed nursery convent and English medium school.<br><br>
                     
                    Most of the nursery schools are run by non Govt. organization so this course can provide you opportunity to get in N.G.O.'s and public school.<br><br>
                    </p>
                  </div>
                </div>
              </div>

              <div class="box">
                <h4>PTT :</h4>
                <div class="boxInfo examInfo">
                  <div>
                    <p style="text-align:justify;">
                      PRIMARY TEACHER TRAINING – 2 YEAR DIPLOMA PROGRAM
                      ELIGIBILITY : INTERMEDIATE PASSED STUDENTS

                      NECCE provides Primary Education for Primary Education. It is a one year Diploma Program for Intermediate Passed Candidates.
                      Primary Education being the very foundation base for entire future life career &amp; Personality Development. Therefore, it is very highly important for Children.
                      The Primary Education lifts the veil of Ignorance &amp; enlightening the mind of the Child wiping out the darkness with the Lamp of Knowledge. Here in, the specially Trained Teachers under NECCE skills locates the hidden talents of each Individual Child and buys them as the surface of the minds and provides environment for their nurture &amp; growth in real life.</p>
                  </div>
                </div>
              </div>

              <div class="box">
                <h4>DFA (DIPLOMA IN FINE ARTS) :</h4>
                <div class="boxInfo examInfo">
                  <div>
                    <p style="text-align:justify;">Diploma (Fine Arts) - Stands for Diploma in Fine  Arts.Typically, a diploma in fine arts is a one year course, minimum eligibility for which is an intermediate (10+2) pass. In the Diploma (Fine Arts) Program students gain the skills and knowledge for manipulating principales and elements of art within the context of visual arts. Subjects Studied under this degree are art business, contemporary art studies, context and culture, life drawing and general drawing.</p>
                  </div>
                </div>
              </div>

              <div class="box">
                <h4>DFA (DIPLOMA IN FINE ARTS)</h4>
                <div class="boxInfo examInfo">
                  <div>
                    <p style="text-align:justify;">Diploma (Fine Arts) - Stands for Diploma in Fine  Arts.Typically, a diploma in fine arts is a one year course, minimum eligibility for which is an intermediate (10+2) pass. In the Diploma (Fine Arts) Program students gain the skills and knowledge for manipulating principales and elements of art within the context of visual arts. Subjects Studied under this degree are art business, contemporary art studies, context and culture, life drawing and general drawing.</p>
                  </div>
                </div>
              </div>

              <div class="box">
                <h4>NPTT(Nursery Primary Training Course)</h4>
                <div class="boxInfo examInfo">
                  <div>
                    <p style="text-align:justify;">SYLLABUS-
                        FIRST YEAR (THEORY)
                        PAPER-1 :- Child Psychology &amp; Child Development
                        PAPER-2 :- Principles of Education &amp; Child Care Education
                        PAPER-3 :-  Nursery School Organisation &amp; Health Education
                        PAPER-4 :- Indian Education System, Structure &amp; Problems
                        PAPER-5 :- Teaching Methods &amp; Teaching Aids
                        PRACTICAL
                        (1)  Teaching Practice and Lesson Planning
                                       (A)  Teaching Practice in Nursery School
                                       (B)  25 Lesson Plan ( 10 Pre-School + 15  1st &amp; 2nd )
                        (2)  Arts And Craft
                                       (A)   Material Aid For Teaching 
                        Charts, clay work, rhymes book, Things with waste material, flash cards, Mask, puppets, colors book etc.
                                       (B)   Black Board Writing:-
                                        Six page of English and Hindi alphabets and                                                  
                                        number writing in capital and running hand.
                                        Five pages of stick- drawing.
                                        Four pages of drawing of common objects and 
                                        fruits.
                                        Four pages of composition drawing.
                                        Preparation of cheap black- board paint and 
                                        chalk.
                                        Black- board as an illustrative aid.
                                        Use of stencils and multiple chalk- holder on 
                                        black board.
                        (3) Child psychology testing
                                       Case study of Nursery child
                                       Case study of K.G. child
                        (4)  Internship work:
                                       (A) Skills development- conduction music 
                                              Sessions, Drama, puppetry and communi-
                                               cation with children/adults.
                                       (B)  Health and nutrition, first aid practical.
                                       (C)  Organization ability of planning and 
                                               organization of field trips, festival celebra-
                                               tion, classroom, management.
                        (5) Assessment
                              Attendance
                              Behaviour
                              Conduct
                              Participation
                        SECOND YEAR:-
                        PAPER-1 :-  Process of Child Learning 
                        PAPER-2 :-  School Organization and management
                        PAPER-3 :-  Health and Physical Education
                        PAPER-4 :-  Computer Science Windows &amp; Ms Office
                        PAPER-5 :-  Environmental Science
                        PAPER-6 :-  Subject Teaching
                        (Maths, Social Studies, Science, General Knowledge, Drawing.)
                        PAPER-7 :-  Language Teaching
                        (Hindi, English, Sanskrit.)
                        PAPER-8 :-  Personality Development
                        PRACTICAL
                        ASSESSMENT</p>
                  </div>
                </div>
              </div>

              <!-- <div class="box">
                <a href="under_construction"><h4>Exam running in classroom</h4></a>
                <div class="boxInfo examInfo">
                  
                  <div>
                    {{HTML::image('images/course/img2.jpg', '')}}
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labor dolore magna aliqua. Ut enim ad minim veniam, quinostrud exercitation ullamco laboris nisi ut aliqu ip ex ea commodo.  </p>
                    <a href="under_construction" class="submit"> Read more </a>
                  </div>
                </div>
              </div>

              <div class="box">
                <a href="under_construction"><h4>A Classroom</h4></a>
                <div class="boxInfo examInfo">
                  
                  <div>
                    {{HTML::image('images/course/img3.jpg', '', array('class'=>"fwidth"))}}
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labor dolore magna aliqua. Ut enim ad minim veniam, quinostrud exercitation ullamco laboris nisi ut aliqu ip ex ea commodo.  </p>
                    <a href="under_construction" class="submit"> Read more </a>
                  </div>
                </div>
              </div> -->
              <ul class="pgNr">
                <li><a href="under_construction"> 1</a></li>
                <li><a href="under_construction"> 2</a></li>
                <li><a href="under_construction"> 3</a></li>
                <li><a href="under_construction"> 4</a></li>
                <li><a href="under_construction"> ></a></li>
              </ul>
                                     
            </div>


            <div id="sidebar" class="column c-33 clearfix">

              

             <div class="courseDetail clearfix">
                <div class="box">
                    
                  <ul class="tabs">
                    <li class="selected"><a href="under_construction">Diploma Course</a></li>
                    <li><a href="under_construction">Certification Course</a></li>
                  </ul>
                  <ul class="content clearfix">
                    <li class="selected">
                      <div class="boxInfo">
                        {{HTML::image('images/course/avatar2.jpg', '')}}
                        <p><a href="under_construction">Donec sed odio dui Maecena sed diam rius</a></p>
                        <span>01 May, 2011</span>
                      </div>
                      <div class="boxInfo">
                        {{HTML::image('images/course/avatar2.jpg', '')}}
                        <p><a href="under_construction">Lorem ipsum dolor sit amet, consectetur</a></p>
                        <span>15 July, 2011</span>
                      </div>
                      <div class="boxInfo">
                        {{HTML::image('images/course/avatar2.jpg', '')}}
                        <p><a href="under_construction">Adipisicing elit, sed do eiusmod tempor incididunt</a></p>
                        <span>22 August, 2011</span>
                      </div>
                    </li>
                    <li>
                      <div class="boxInfo">
                      {{HTML::image('images/course/avatar2.jpg', '')}}
                        <p><a href="under_construction">Sed ut perspiciatis unde omnis iste</a></p>
                        <span>04 July 2013</span>
                      </div>
                      <div class="boxInfo">
                        {{HTML::image('images/course/avatar2.jpg', '')}}
                        <p><a href="under_construction">1914 translation by H. Rackham</a></p>
                        <span>22 May, 2013</span>
                      </div>
                      <div class="boxInfo">
                        {{HTML::image('images/course/avatar2.jpg', '')}}
                        <p><a href="under_construction">At vero eos et accusamus et iusto</a></p>
                        <span>15 December 2014</span>
                      </div>
                    </li>
                  </ul>

                </div>
            </div>

            <div class="clearfix">
                <div class="box">
                  <h4>Archives</h4>
                  <div class="boxInfo archives">
                    <ul>
                      <li><a href="under_construction">March 2012</a></li>
                      <li><a href="under_construction">February 2012</a></li>
                      <li><a href="under_construction">October 2011</a></li>
                      <li><a href="under_construction">August 2011</a></li>
                      <li><a href="under_construction">June 2011</a></li>
                    </ul>
                  </div>
                </div>
              </div>

            <!-- <div class="links column c-40 clearfix">
              <h3>Quick Links</h3>
              <ul class="cContent clearfix">
                <li><a href="under_construction">Student Verification</a></li>
                <li><a href="under_construction">Query Form</a></li>
                <li><a href="under_construction">Date Sheet</a></li>
                <li><a href="under_construction">Governing Body</a></li>
                <li><a href="under_construction">Downloads</a></li>
                <li><a href="under_construction">Centers</a></li>
              </ul>
            </div>         -->
            <div class="links column c-33 clearfix">
            <h3>Quick Links</h3>
              <ul class="cContent clearfix">
                <li><a href="#" id="student-verfication">Student Verification</a></li>
                <li><a href="#" id="query-form">Query Form</a></li>
                <li><a href="#" id="date-sheet">Date Sheet</a></li>
                <li><a href="#" id="governing-body">Governing Body</a></li>
                <li><a href="#" id="download">Downloads</a></li>
                <li><a href="#" id="centers">Centers</a></li>
              </ul>
          </div>
                         
          </div> 

        </div>
      </div>
    </div>
   @stop