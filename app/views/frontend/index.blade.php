@extends('frontend/layout/master')
	@section('content')
		<div id="contentBk">
			<div id="content" class="clearfix">
				<div id="homeSlider" class="clearfix flexslider">
					<div class="thumbs"></div>
						<ul class="slides">
							<li data-thumb="{{public_path() . '/images/slider/1st_thumb.jpg'}}">
								{{HTML::image('images/slider/1st.jpg', 'A Classroom')}}
							</li>
							<li data-thumb="{{public_path() . '/images/slider/2nd_thumb.jpg'}}">
								{{HTML::image('images/slider/2nd.jpg', 'Just a different perspective over this new issue')}}
							</li>
							<li data-thumb="{{public_path() . '/images/slider/3rd_thumb.jpg'}}">
								{{HTML::image('images/slider/3rd.jpg', 'A Classroom')}}
							</li>
							<li data-thumb="{{public_path() . '/images/slider/4th_thumb.jpg'}}">
								{{HTML::image('images/slider/4th.jpg', 'Just a different perspective over this new issue')}}
							</li>
						</ul>

						<ul class="captions">
							<li>
								<h3>A student <strong>reading</strong></h3>
								<p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo, perspiciatis unde omnis.</p>
							</li>
							<li>
								<h3>Just a different <em>perspective</em> over this new issue</h3>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</li>
							<li>
								<h3>Look at <strong>the baloon!</strong></h3>
								<p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo, perspiciatis unde omnis.</p>
							</li>
							<li>
								<h3>90% of the people have back issues</h3>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</li>
						</ul>
					</div>

				<div class="wrapper">
					<div class="welcome column c-67 clearfix">
						<h3>Welcome to GECCE</h3>
						<div class="cContent clearfix">
							{{HTML::image('images/welcome.jpg', "")}}
							<div>
								<p>Global early childhood care &amp; education is registered under rules and regulations of govt. of Delhi act XXI, 1860. We aim to provide vocational course to educated unemployment student to enable them to get better employment and live a peaceful and respectable life with the motto of early childhood education. {{ link_to_route('about', ' Read more') }}</p>	
							</div>
						</div>
					</div>
					<div class="searchCourse searchCourseHome column c-33 clearfix">
						<p>Ask Us</p>
						<form action="under_construction" class="form-enquiry" id="form-enquiry" name="form-enquiry">
							<input class="focus input" type="text" name="name" placeholder="Name" />
							<input class="focus input" type="text" name="email" placeholder="Email" />
							<input class="focus input" type="text" name="phone" placeholder="Phone" />
							<textarea class="focus input" name="query" placeholder="Ask your query"></textarea>
							<input class="submit" type="submit" value="Submit"/>
						</form>
					</div>

					<div class="clear"></div>

					<div class="tour column c-33 clearfix">
						<h3>Thought of the day</h3>
						<div class="arrows"></div>
						<div class="cContent clearfix">
							<ul>
								<li><p>For any kind of verification like Student / Registration/ I-Card / Mark Sheet / Diploma or any other document, send an application on a plain paper along with photocopy of the document and a Demand Draftof Rs. 300/- in favour of Global Early Childhood Care &amp; Education, Payble At New Delhi.</p></li>
								<li><p>&nbsp;</p><p>We will Send the VerficationDetails by Post.</p></li>
							</ul>
							<!-- <ul class="slides">
							  <li>
								<iframe class="fwidth" src="http://player.vimeo.com/video/34015882?title=0&amp;byline=0&amp;portrait=0&amp;color=4496D2" width="303" height="170"  webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
								  <p>Lorem ipsum dolor sit amet, consectetur sit amet
								  eiusmod tempor incididunt ut labore.</p>
							  </li>
							  <li>
								<iframe class="fwidth" src="http://player.vimeo.com/video/32061310?title=0&amp;byline=0&amp;portrait=0&amp;color=4496D2" width="303" height="170" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
								  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							  </li>
							  <li>
								<iframe class="fwidth" src="http://player.vimeo.com/video/55472286?title=0&amp;byline=0&amp;portrait=0&amp;color=4496D2" width="303" height="170" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
								  <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
							  </li>
							</ul> -->
						</div>    
					</div>

					<div class="news column c-33 clearfix">
						<h3>Recent News</h3>
						<div class="arrows"></div>
						<div class="cContent clearfix rotator">
							<ul class="slides">
								<li>
									<div class="post">
										{{HTML::image('images/other/ntt.gif', '')}}
										<div class="info">
											{{ link_to_route('under_construction', 'Nursery Teacher Traning(NTT)') }}
											<span>Posted on 07 Jan 2014</span>
											<p>According to Indian Govt. new education policy that it is necessary to appoint trained teacher in every school.</p>
										</div>
									</div>
									<div class="post">
										{{HTML::image('images/other/ptt.gif', '')}}
										<div class="info">
											<!-- <a href="#"><h5>Primary Teacher Traning(PTT)</h5></a> -->
											{{ link_to_route('under_construction', 'Primary Teacher Traning(PTT)') }}
											<span>Posted on 07 Jan 2014</span>
											<p>Primary Teacher Training is required training if you are going for role as primary school teacher in any middle school in World.</p>
										</div>
									</div>
								</li>
								<li>
									<div class="post">
										{{HTML::image('images/other/computer_application.gif', '')}}
										<div class="info">
											{{ link_to_route('under_construction', 'Diploma In Computer Application') }}
											<!-- <a href="#"><h5>Diploma In Computer Application</h5></a> -->
											<span>Posted on 07 Jan 2014</span>
											<p>Primary Teacher Training is required training if you are going for role as primary school teacher in any middle school in World.</p>
										</div>
									</div>
									<div class="post">
										{{HTML::image('images/other/d.ed.gif', '')}}
										<div class="info">
											{{ link_to_route('under_construction', 'Diploma In Education(D.ed)') }}
											<!-- <a href="#"><h5>Diploma In Education(D.ed)</h5></a> -->
											<span>Posted on 07 Jan 2014</span>
											<p>Diploma in Education Examination is held once a year for 1st year &amp; 2nd year Regular and Private candidates.</p>
										</div>
									</div>
								</li>
								<!-- <li>
									<div class="post">
										{{HTML::image('images/other/news.png', '')}}
										<div class="info">
											<a href="#"><h5>Admissions open for 2014</h5></a>
											<span>Posted on 07 Jan 2014</span>
											<p>Lorem ipsum dolor sit amet eiusmod tempor labore..</p>
										</div>
									</div>
									<div class="post">
										{{HTML::image('images/other/news.png', '')}}
										<div class="info">
											<a href="#"><h5>Admissions open for 2014</h5></a>
											<span>Posted on 07 Jan 2014</span>
											<p>Lorem ipsum dolor sit amet eiusmod tempor labore..</p>
										</div>
									</div>
								</li>
								<li>
									<div class="post">
										{{HTML::image('images/other/news.png', '')}}
										<div class="info">
											<a href="#"><h5>Admissions open for 2014</h5></a>
											<span>Posted on 07 Jan 2014</span>
											<p>Lorem ipsum dolor sit amet eiusmod tempor labore..</p>
										</div>
									</div>
									<div class="post">
										{{HTML::image('images/other/news.png', '')}}
										<div class="info">
											<a href="#"><h5>Admissions open for 2014</h5></a>
											<span>Posted on 07 Jan 2014</span>
											<p>Lorem ipsum dolor sit amet eiusmod tempor labore..</p>
										</div>
									</div>
								</li> -->
							</ul>
						</div>
					</div>

					<div class="links column c-33 clearfix">
						<h3>Quick Links</h3>
						<ul class="cContent clearfix">
							<li><a href="#" id="student-verfication">Student Verification</a></li>
							<li><a href="#" id="query-form">Query Form</a></li>
							<li><a href="#" id="date-sheet">Date Sheet</a></li>
							<li><a href="#" id="governing-body">Governing Body</a></li>
							<li><a href="#" id="download">Downloads</a></li>
							<li><a href="#" id="centers">Centers</a></li>
						</ul>
					</div>

					<div class="clear"></div>

					<div class="event column c-67 clearfix">
						<h3>Governing Body</h3>
						<div class="arrows"></div>
						<div class="cContent clearfix rotator">
							<ul class="slides">
								<li><p>Mr.P k Lodhi</p><i>Secratory</i></li>
								<li><p>Mr.S k Kaushal</p><i>Depty Director</i></li>
								<li><p>Mr.Saheb Singh</p><i>Director</i></li>
								<li><p>Mr.Nisha Verma</p><i>Registrar</i></li>
								<li><p>Mr.Kalpna Verma</p><i>Examination Controller</i></li>
								<li><p>Mr.Santosh Sharma</p><i>Co-ordinator(Aligarh)</i></li>
								<li><p>Mr.Krishan Kant</p><i>Zonal Co-ordinator Aligarh</i></li>
								<li><p>Mr.Babita Kaushal</p><i>Panel Controller(operations)</i></li>
								<!-- <li><a href="img/events/e1_large.jpg" rel="lightbox[events]" class="grayColor"><img data-color="img/events/e1.jpg" src="img/events/e1_gray.jpg" alt="" class="imgBorder"></a></li>
								<li><a href="img/events/e2_large.jpg" rel="lightbox[events]" class="grayColor"><img data-color="img/events/e2.jpg" src="img/events/e2_gray.jpg" class="imgBorder" alt=""></a></li>
								<li><a href="img/events/e3_large.jpg" rel="lightbox[events]" class="grayColor"><img data-color="img/events/e3.jpg" src="img/events/e3_gray.jpg" alt="" class="imgBorder"></a></li>
								<li><a href="img/events/e4_large.jpg" rel="lightbox[events]" class="grayColor"><img data-color="img/events/e4.jpg" src="img/events/e4_gray.jpg" class="imgBorder" alt=""></a></li>
								<li><a href="img/events/e5_large.jpg" rel="lightbox[events]" class="grayColor"><img data-color="img/events/e5.jpg" src="img/events/e5_gray.jpg" class="imgBorder" alt=""></a></li>
								<li><a href="img/events/e6_large.jpg" rel="lightbox[events]" class="grayColor"><img data-color="img/events/e6.jpg" src="img/events/e6_gray.jpg" class="imgBorder" alt=""></a></li> -->
							</ul>
						</div>
					</div>

					<div class="featured column c-33 clearfix">
						<h3>Certifications</h3>
						<div class="cContent">
							{{HTML::image('images/ntt.jpg', '', ['class' => 'imgBorder'])}}
							<div>
								<h5>NTT</h5>
								<p>According to Indian Govt. new education... </p>
								<!-- <a href="#/">Read more</a> -->
								{{ link_to_route('under_construction', ' Read more') }}
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	@stop