@extends('frontend/layout/master')
  @section('content')
    <div id="contentBk" class="clearfix">
      <div id="content">
        <div class="topImg clearfix">
    
          {{HTML::image('images/headers/header_1.jpg', 'About Us')}}
          <p>About <strong>Us</strong></p>
        </div>
        <div class="wrapper">
          <div class="column c-67 clearfix">
            <div class="box">
              <h4>Welcome to GECCE</h4>
              <div class="boxInfo">
      
                {{HTML::image('images/welcome.jpg', '')}}
                <div>
                    <p>Global early childhood care &amp; education is registered under rules and regulations of govt. of Delhi act XXI, 1860. We aim to provide vocational course to educated unemployment student to enable them to get better employment and live a peaceful and respectable life with the motto of early childhood education.</p>
                    <p>It is the in line with research work at NCERT.we are following the rules and regulations of NCERT, NCTE and NCVT to strive for excellence by adopting latest educational policy. This too is the only genuine bodies for guidness and affiliation. At present various study centre are running in different state of India under the able guidance of NCERT, education expert as a nongovernmental organization (NGO), our institute is trying to develop human resource to make successful the vocational training program , by imparting NTT for the pre primary school. </p>
                </div>
              </div>
            </div>
          </div>
          <div class="searchCourse column c-33 clearfix">
            <p>Ask Us</p>
            <form action='under_construction' class="form-enquiry" id="form-enquiry" name="form-enquiry">
              <input class="focus input" type="text" name="name" placeholder="Name" />
              <input class="focus input" type="text" name="email" placeholder="Email" />
              <input class="focus input" type="text" name="phone" placeholder="Phone" />
              <textarea class="focus input" name="query" placeholder="Ask your query"></textarea>
              <input class="submit" type="submit" value="Submit"/>
            </form>
          </div>

          <div>
            <h2 style="color:#66973E;">Our Objectives</h2>
            <ul class="cContent clearfix">
              <li><p> Delivering Education at the doorsteps of the candidates at distant places.</p></li>
              <li><p> * To Provide Job-Oriented and Professional Courses for youth enabling them for getting suitable employments.</p></li>
              <li><p> * To Promote Education under Open &amp; Distance Education System on the International Pattern for near &amp; remote areas as well.</p></li>
              <li><p> * To train skilled teachers in latest Technology Application for daily life.</p></li>
              <li><p> * To train candidates in State of Art Technologies of Teaching &amp; Guidance.</p></li>
              <li><p> * To enable youth to be productive &amp; useful citizen in the fast changing globalization of the world.</p></li>
            </ul>
          </div>

          <div class="heading">
            <h3 class="title">Popular Courses</h3>
          </div>
          
          <div class="clear"></div>
          <div class="column c-33 clearfix">
            <div class="box">
              <h4><strong>Course one</strong></h4>
              <div class="boxInfo">
                {{HTML::image('images/other/ntt.gif', '')}}
                <h6>Nursery Teacher Traning(NTT)</h6>
                <p>According to Indian Govt. new education policy that it is necessary to appoint trained teacher in every school.</p>
              </div>
            </div>
          </div>
          <div class="column c-33 clearfix">
            <div class="box">
              <h4><strong>Course two</strong></h4>
              <div class="boxInfo">
      
                {{HTML::image('images/other/ptt.gif', '')}}
                <h6>Primary Teacher Traning(PTT)'</h6>
                <p>Primary Teacher Training is required training if you are going for role as primary school teacher in any middle school in World.</p>
              </div>
            </div>
          </div>
          <div class="column c-33 clearfix">
            <div class="box">
              <h4><strong>Course three</strong></h4>
              <div class="boxInfo">
                {{HTML::image('images/other/computer_application.gif', '')}}
                <h6>Diploma In Computer Application</h6>
                <p>Primary Teacher Training is required training if you are going for role as primary school teacher in any middle school in World.</p>
              </div>
            </div>
          </div>
        </div>
          <!-- <div class="column c-33 clearfix">
            <div class="box">
              <h4><strong>Course one</strong></h4>
              <div class="boxInfo">
      
                {{HTML::image('images/professors/p1.jpg', '', array('class' => 'fwidth'))}}
                <h6>Maths professor</h6>
                <p>Lorem ipsum doloreiusmod tempor dolorei doloreiusmod tempor usmod tempordolor eiusmod tempor...</p>
              </div>
            </div>
          </div>
          <div class="column c-33 clearfix">
            <div class="box">
              <h4><strong>Course one</strong></h4>
              <div class="boxInfo">
      
                {{HTML::image('images/professors/p2.jpg', '', array('class' => 'fwidth'))}}
                <h6>Chemestry professor</h6>
                <p>Lorem ipsum doloreiusmod tempor dolorei doloreiusmod tempor usmod tempordolor eiusmod tempor...</p>
              </div>
            </div>
          </div>
          <div class="column c-33 clearfix">
            <div class="box">
              <h4><strong>Course one</strong></h4>
              <div class="boxInfo">
      
                {{HTML::image('images/professors/p3.jpg', '', array('class' => 'fwidth'))}}
                <h6>Communication professor</h6>
                <p>Lorem ipsum doloreiusmod tempor dolorei doloreiusmod tempor usmod tempordolor eiusmod tempor...</p>
              </div>
            </div>
          </div>
        </div> -->


      </div>
    </div>
    @stop