@extends('frontend/layout/master')
  @section('content')
  	<div id="contentBk" class="clearfix">
      <div id="content">
          <div class="topImg clearfix">
              {{HTML::image('images/headers/header_1.jpg', 'About Us')}}
              <p>Features</p>
          </div>
        <div class="wrapper grid">

            <div class="column c-100 clearfix">
                <h2>Grid System</h2>
            </div>

            <div class="column c-25 clearfix">
                <h3><span>01</span>One Fourth</h3>
                <p>Although starting a prototype on a is sometimes easier, it’s not the best visually problem-solve. When you need to ideate website layouts or mobile applications or to storyboar workflows and context....</p>
            </div>
            <div class="column c-25 clearfix">
                <h3><span>02</span>One Fourth</h3>
                <p>Although starting a prototype on a is sometimes easier, it’s not the best visually problem-solve. When you need to ideate website layouts or mobile applications or to storyboar d context.....</p>
            </div>
            <div class="column c-25 clearfix">
                <h3><span>03</span>One Fourth</h3>
                <p>Although starting a prototype on a is sometimes easier, it’s not the best visually problem-solve. When you need to ideate website layouts or mobile applications or to storyboar workflows and context....</p>
            </div>
            <div class="column c-25 clearfix">
                <h3><span>04</span>One Fourth</h3>
                <p>Although starting a prototype on a is sometimes easier, it’s not the best visually problem-solve. When you need to ideate website layouts or mobile applications or to storyboar workflows and context....</p>
            </div>
        
            <div class="clear"></div>

           <div class="column c-50 clearfix">
                <h3><span>01</span>One Half</h3>
                <p>Although starting a prototype on a is sometimes easier, it’s not the best visually problem-solve. When you need to ideate website layouts or mobile applications or to storyboar workflows and context....</p>
            </div>
            <div class="column c-50 clearfix">
                <h3><span>02</span>One Half</h3>
                <p>Although starting a prototype on a is sometimes easier, it’s not the best visually problem-solve. When you need to ideate website layouts or mobile applications or to storyboar workflows and context....</p>
            </div>

            <div class="clear"></div>

            <div class="column c-33 clearfix">
                <h3><span>01</span>One Thrid</h3>
                <p>Although starting a prototype on a is sometimes easier, it’s not the best visually problem-solve. When you need to ideate website layouts or mobile applications or to storyboar workflows and context....</p>
            </div>
            <div class="column c-33 clearfix">
                <h3><span>02</span>One Thrid</h3>
                <p>Although starting a prototype on a is sometimes easier, it’s not the best visually problem-solve. When you need to ideate website layouts or mobile applications or to storyboar workflows and context....</p>
            </div>
            <div class="column c-33 clearfix">
                <h3><span>03</span>One Thrid</h3>
                <p>Although starting a prototype on a is sometimes easier, it’s not the best visually problem-solve. When you need to ideate website layouts or mobile applications or to storyboar workflows and context....</p>
            </div>

            <div class="clear"></div>

            <div class="column c-100 clearfix">
                <div class="box">
                    <h4>Accordion</h4>
                    <div class="boxInfo">
                        <div class="accordion">
                            <div class="box">
                                <a href="#">Accordion 1</a>
                                <div class="boxInfo acText">
                                    <p> If you are approached and persuaded to hire a particular attorney or health care and persuaded to hire a particular attorney or healt provider, you may be a victim of illegal solicitation to hire a particular attorney or and persuaded to hire a particular attorney or healt health care provider, you may be a victim of illegal solicitation.you are approached and persuaded to hire a particular attorney or health care and persuaded to hire a particular attorney or healt provider, you may be. 
                                    </p>
                                </div>
                            </div>
                            <div class="box">
                                <a href="#">Accordion 2</a>
                                <div class="boxInfo acText">
                                    <p> If you are approached and persuaded to hire a particular attorney or health care and persuaded to hire a particular attorney or healt provider, you may be a victim of illegal solicitation to hire a particular attorney or and persuaded to hire a particular attorney or healt health care provider, you may be a victim of illegal solicitation.you are approached and persuaded to hire a particular attorney or health care and persuaded to hire a particular attorney or healt provider, you may be. 
                                    </p>
                                </div>
                            </div>
                            <div class="box">
                                <a href="#">Accordion 3</a>
                                <div class="boxInfo acText">
                                    <p> If you are approached and persuaded to hire a particular attorney or health care and persuaded to hire a particular attorney or healt provider, you may be a victim of illegal solicitation to hire a particular attorney or and persuaded to hire a particular attorney or healt health care provider, you may be a victim of illegal solicitation.you are approached and persuaded to hire a particular attorney or health care and persuaded to hire a particular attorney or healt provider, you may be. 
                                    </p>
                                </div>
                            </div>
                            <div class="box">
                                <a href="#">Accordion 4</a>
                                <div class="boxInfo acText">
                                    <p> If you are approached and persuaded to hire a particular attorney or health care and persuaded to hire a particular attorney or healt provider, you may be a victim of illegal solicitation to hire a particular attorney or and persuaded to hire a particular attorney or healt health care provider, you may be a victim of illegal solicitation.you are approached and persuaded to hire a particular attorney or health care and persuaded to hire a particular attorney or healt provider, you may be. 
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="column c-100 clearfix">
                <div class="box">
                    <h4>Toggle</h4>
                    <div class="boxInfo">
                        <div class="boxInfo toggleInfo">
                            <a href="#">Although starting a prototype on a computer is sometimes easier, it's not the best way on a computer is sometimes easie prototype on a  easie...</a>
                            <div><p> Ut felis. Praesent dapibus, neque id cursus faucibus adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua raesent dabus, faucibus adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua </p></div>
                        </div>
                        <div class="boxInfo toggleInfo">
                            <a href="#">Although starting a prototype on a computer is sometimes easier, it's not the best way on a computer is sometimes easie prototype on a  easie...</a>
                            <div><p> Ut felis. Praesent dapibus, neque id cursus faucibus adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua raesent dabus, faucibus adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua </p></div>
                        </div>
                        <div class="boxInfo toggleInfo">
                            <a href="#">Although starting a prototype on a computer is sometimes easier, it's not the best way on a computer is sometimes easie prototype on a  easie...</a>
                            <div><p> Ut felis. Praesent dapibus, neque id cursus faucibus adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua raesent dabus, faucibus adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua </p></div>
                        </div>
                        <div class="boxInfo toggleInfo">
                            <a href="#">Although starting a prototype on a computer is sometimes easier, it's not the best way on a computer is sometimes easie prototype on a  easie...</a>
                            <div><p> Ut felis. Praesent dapibus, neque id cursus faucibus adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua raesent dabus, faucibus adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua </p></div>
                        </div>
                    </div>
                </div>   
            </div>

            <div class="column c-100 clearfix">
                <div class="box">
                    <h4>Dropcap</h4>
                    <div class="boxInfo">
                        <p><span class="dropcap">A</span>ough starting a prototype on a computer is sometimes easier, it's not the best way to visually problem-solve. When you need to ideate website layouts hough starting a prototype on a computer is sometimes easier, it's not the best way to visually problem-solve. When you need to ideate website layo applications a prototype on a computer is sometimes easier, it's not the best way to visually problem-solve. When you need to ideate website layouts or.. </p>
                    </div>
                </div>
            </div>

            <div class="column c-100 clearfix">
                <div class="box">
                    <h4>Blockquote</h4>
                    <div class="boxInfo">
                        <blockquote class="clearfix"><p><span class="blockquote1">"</span><em>Aough starting a prototype on a computer is sometimes easier, it's not the best way to visually problem-solve. When you need to ideate website layouts hough starting a prototype on a computer is sometimes easier, it's not the best way to visually problem-solve. When you need to ideate website layo applications a prototype on a computer is sometimes easier, it's not the best way to visually problem-solve. When you need to ideate website layouts or..</em><span class="blockquote2">"</span></p></blockquote>
                    </div>
                </div>
            </div>

            <div class="column c-100 clearfix" style="margin-bottom:-20px;">
                <h2>List Style</h2>
            </div>

            <div class="column c-25 clearfix">
                <ul class="cList arrow1Li">
                    <li>Lonsectetur adipisicing</li>
                    <li>sed do eiusmod</li>
                    <li>Lonsectetur adipisicing</li>
                    <li>sed do eiusmod tempor</li>
                </ul>
            </div>

            <div class="column c-25 clearfix">
                <ul class="cList pointLi">
                    <li>Lonsectetur adipisicing</li>
                    <li>sed do eiusmod</li>
                    <li>Lonsectetur adipisicing</li>
                    <li>sed do eiusmod tempor</li>
                </ul>
            </div>

            <div class="column c-25 clearfix">
                <ul class="cList checkLi">
                    <li>Lonsectetur adipisicing</li>
                    <li>sed do eiusmod</li>
                    <li>Lonsectetur adipisicing</li>
                    <li>sed do eiusmod tempor</li>
                </ul>
            </div>

            <div class="column c-25 clearfix">
                <ul class="cList arrow2Li">
                    <li>Lonsectetur adipisicing</li>
                    <li>sed do eiusmod</li>
                    <li>Lonsectetur adipisicing</li>
                </ul>
            </div>

        </div>
      </div>
    </div>
   @stop