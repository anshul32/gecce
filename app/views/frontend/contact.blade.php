@extends('frontend/layout/master')
  @section('content')
    <div id="contentBk" class="clearfix">
      <div id="content">
        <div class="topImg clearfix">
          {{HTML::image('images/headers/header_1.jpg', 'About Us')}}
          <p>Contact <strong>Us</strong></p>
        </div>
        <div class="wrapper">
          <div class="column c-67 clearfix">
            <div class="box">
              <h4><strong>Contact</strong> Us</h4>
              <div class="boxInfo"><iframe class="fwidth" width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d448182.5073807705!2d77.0932634!3d28.646965499999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sB-1291%2C+Ist+Floor%2C+G.D.Colony%2C+Mayur+Vihar!5e0!3m2!1sen!2sin!4v1414224908814"></iframe>
              </div>
            </div>
          </div>

          <div class="column c-33 last  clearfix">
            <div class="box contactUs">
              <h4><span>We are here</span></h4>
              <div class="boxInfo">
                <p>Globle Early Childhood Care &amp; Education</p>
                <p>B-1291, Ist Floor,<br> G.D.Colony, Mayur Vihar <br>Delhi, India <br><br><strong><a href="mailto:enquiry@gecce.co.in">enquiry@gecce.co.in</a><br> +91-886 069 1116</strong></p> 
                
              </div>
            </div>
          </div>

          <div class="clear"></div>

          <div class="column c-67 clearfix">
            <div class="box contactUs ">
              <h4><span>Get in touch</span></h4>
                <div class="boxInfo contactForm">
                    <form id="contact" action="under_construction">
                        <div>
                            <label>Name:</label>
                            <input id="contactName"type="text"/>
                        </div>
                        <div>
                            <label>Email:</label>
                            <input id="contactMail" type="text"/>
                        </div>
                        <div>
                            <label>Message:</label>
                            <textarea id="contactMessage" type="text"></textarea>
                        </div>
                        <div>
                        <input id="contactSubmit" class="submit" type="submit" value="Submit"/>
                    </div>
                </form>
                <p id="contactSuccess" class="hidden">Your message was successfuly sent! Please wait up to 24hrs until we can contact you back!</p>
                <p id="contactError" class="hidden">Please complete all the required fields properly!</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @stop