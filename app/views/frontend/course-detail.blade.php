@extends('frontend/layout/master')
  @section('content')
    <div id="contentBk" class="clearfix">
      <div id="content">
        <div class="topImg clearfix">
          {{HTML::image('images/headers/header_2.jpg', 'About Us')}}
          <p>Our <strong>Course</strong></p>
        </div>
        <div class="wrapper">
          <div class="course column c-67 clearfix">
            <div class="box">
              <h4>Take light shoes who cares your leg</h4>
              <div class="boxInfo examInfo">
                  
                  <div>
                      {{HTML::image('images/course/img3.jpg', '')}}
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labor dolore magna aliqua. Ut enim ad minim veniam, quinostrud exercitation ullamco laboris nisi ut aliqu    ip ex ea commodo. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i
                      dolore magna aliqua. Ut enim ad minim veniam, quinostrud exercitation ullamco laboris nisi ut aliqu
                      ip ex ea commodo.</p>

                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labor
                      dolore magna aliqua. Ut enim ad minim veniam, quinostrud exercitation ullamco laboris nisi ut aliqu
                      ip ex ea commodo. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i
                      dolore magna aliqua.   </p>
                      <iframe class="fbLike" src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fthemeforest.net&amp;send=false&amp;layout=standard&amp;width=320&amp;show_faces=false&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=129471183873981" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:320px; height:30px;" allowTransparency="true"></iframe>
                  </div>
              </div>
            </div>

            
          </div>                              
        

          <div id="sidebar" class="column c-33 clearfix">

              

             <div class="courseDetail clearfix">
                <div class="box">
                    
                  <ul class="tabs">
                    <li class="selected"><a href="#">Diploma Course</a></li>
                    <li><a href="#">Certification Course</a></li>
                  </ul>
                  <ul class="content clearfix">
                    <li class="selected">
                      <div class="boxInfo">
                        {{HTML::image('images/course/avatar2.jpg', '')}}
                        <p><a href="course-detail.html">Donec sed odio dui Maecena sed diam rius</a></p>
                        <span>01 May, 2011</span>
                      </div>
                      <div class="boxInfo">
                        {{HTML::image('images/course/avatar2.jpg', '')}}
                        <p><a href="course-detail.html">Lorem ipsum dolor sit amet, consectetur</a></p>
                        <span>15 July, 2011</span>
                      </div>
                      <div class="boxInfo">
                        {{HTML::image('images/course/avatar2.jpg', '')}}
                        <p><a href="course-detail.html">Adipisicing elit, sed do eiusmod tempor incididunt</a></p>
                        <span>22 August, 2011</span>
                      </div>
                    </li>
                    <li>
                      <div class="boxInfo">
                      {{HTML::image('images/course/avatar2.jpg', '')}}
                        <p><a href="course-detail.html">Sed ut perspiciatis unde omnis iste</a></p>
                        <span>04 July 2013</span>
                      </div>
                      <div class="boxInfo">
                        {{HTML::image('images/course/avatar2.jpg', '')}}
                        <p><a href="course-detail.html">1914 translation by H. Rackham</a></p>
                        <span>22 May, 2013</span>
                      </div>
                      <div class="boxInfo">
                        {{HTML::image('images/course/avatar2.jpg', '')}}
                        <p><a href="course-detail.html">At vero eos et accusamus et iusto</a></p>
                        <span>15 December 2014</span>
                      </div>
                    </li>
                  </ul>

                </div>
            </div>

            <div class="clearfix">
                <div class="box">
                  <h4>Archives</h4>
                  <div class="boxInfo archives">
                    <ul>
                      <li><a href="#">March 2012</a></li>
                      <li><a href="#">February 2012</a></li>
                      <li><a href="#">October 2011</a></li>
                      <li><a href="#">August 2011</a></li>
                      <li><a href="#">June 2011</a></li>
                    </ul>
                  </div>
                </div>
              </div>

            <div class="links column c-40 clearfix">
              <h3>Quick Links</h3>
              <ul class="cContent clearfix">
                <li><a href="#">Student Verification</a></li>
                <li><a href="#">Query Form</a></li>
                <li><a href="#">Date Sheet</a></li>
                <li><a href="#">Governing Body</a></li>
                <li><a href="#">Downloads</a></li>
                <li><a href="#">Centers</a></li>
              </ul>
            </div>        
                         
          </div>  

        </div>
      </div>
    </div>
  @stop
