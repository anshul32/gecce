<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js ie ie8"> <![endif]-->
<!--[if IE 9]>         <html class="no-js ie ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Global Early Child Care &amp; Education</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />

        {{HTML::style('css/main.css')}}
        
        <!--[if lt IE 9]>
         <script src="js/html5shiv.js"></script>
        <![endif]-->
    </head>
    
    <body>
    <!-- SITE CONTENT -->
    <header>
        <div class="parentContainer">
            <div id="socialButtons">
                <a href="#" id="student-login">
                    <p>Student Login </p>
                </a>
                <a href="#" id="branch-login">
                    <p>Branch Login </p>
                </a>
                <div class="notify-coming-soon barber-notify hide">
                    COMING SOON! YAY. Please stay tuned
                </div>
            </div>
        </div>
        
        <div>
            <div id="menu">
                <section>
                    <a href="#">{{HTML::image('images/logo.png', '')}}</a>
                        <ul class="clearfix">
                            <li class="selected">{{ link_to_route('home', 'Home') }}</li>
                            <li>{{ link_to_route('about', 'About') }}</li>
                            <li>{{ link_to_route('course', 'Course') }}</li>
                            <li>{{ link_to_route('student_zone', 'Student Zone') }}</li>
                            <li>{{ link_to_route('syllabus', 'Syllabus') }}</li>
                            <li>{{ link_to_route('contact', 'Contact Us') }}</li>
                            <!-- <li class="selected"><a href="#">Home</a></li> -->
                            <!-- <li><a href="#">About</a></li> -->
                            <!-- <li><a href="#">Course</a></li>
                            <li><a href="#">Student Zone</a></li>
                            <li><a href="#">Syllabus</a></li>
                            <li><a href="#">Contact Us</a></li> --> 
                        </ul>
                </section>
            </div>
            <div id="tagline">
                <section>
                    <a href="under_construction">“ Admission Open for 2015 click here to <strong>Get Form</strong> ”</a>
                </section>
            </div>
            <div id="info">
                <section>
                    <div>
                        {{HTML::image('images/phone.png', '')}}
                        <h2>+91-8860691116</h2>
                        <h4>B-1291, Ist Floor, G.D.Colony, Mayur Vihar</h4>
                    </div>
                    <span class="element leftElement"></span>
                    <span class="triangle">triangle</span>
                </section>
            </div>               
        </div>    
    </header>   