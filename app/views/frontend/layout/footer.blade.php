<footer>
    <div class="stripe clearfix">
        <div class="twitter">
          {{HTML::image('images/twitter.png', '')}}
          <div class="twitterList" data-user="rubenbristian"></div>
        </div>
    </div>
    <div class="mail">
        <div>
        	<div class="mailInfo">
            	<div>
					<h4>Student Verification</h4>
					<input id="mailInput" type="text" />
					<input id="mailSubmit" type="submit" value="Go"/>
	            </div>
            </div>
			<span class="triangle">triangle</span>
			<span class="element rightElement"></span>
        </div>
    </div> 

    <div id="footerContent" >
        <section>
    	    <div class="clearfix">
            	<div class="links column c-30 clearfix ">
              		<h3>Links</h3>
              		<ul>
		                <!-- <li><a href="#">Home</a></li>
		                <li><a href="#">About</a></li>
		                <li><a href="#">Courses</a></li>
		                <li><a href="#">Syllabus</a></li> -->
                    <li>{{ link_to_route('home', 'Home') }}</li>
                    <li>{{ link_to_route('about', 'About') }}</li>
                    <li>{{ link_to_route('course', 'Course') }}</li>
                    <li>{{ link_to_route('syllabus', 'Syllabus') }}</li>
              		</ul>
            	</div>
            	<div class="news column c-40 clearfix">
					<h3>Popular Courses</h3>
					<div class="clearfix">
                		<div class="post clearfix">
                  			{{HTML::image('images/other/ntt.gif', '')}}
                  			<div class="info">
                    			<!-- <a href="#"><h5>Admissions open for 2014</h5></a>
                    			<span>Posted on 07 jan 2014</span>
                    			<p>Lorem ipsum dolor sit amet set..</p> -->
                    			<span class="coming-soon-text">{{ link_to_route('under_construction', 'Nursery Teacher Traning(NTT)') }}</span>
                  			</div>
                		</div>
                		<div class="post clearfix ">
                  			{{HTML::image('images/other/ptt.gif', '')}}
              				<div class="info">
                				<!-- <a href="#"><h5>Admissions open for 2014</h5></a>
                				<span>Posted on 07 jan 2014</span>
                				<p>Lorem ipsum dolor sit amet set..</p> -->
                				<span class="coming-soon-text">{{ link_to_route('under_construction', 'Primary Teacher Traning(PTT)') }}</span>
              				</div>
            			</div>
          			</div>
        		</div>

				<div class="contact column c-30 clearfix ">
					<h3>Contact Form</h3>
					<input id="name" type="text" value="Name" />
					<input id="mail" type="text" value="Email" />
					<textarea id="message">Message</textarea>
					<input id="submit" class="submit" type="submit" value="Submit"/>
				</div>
    		</div>
    		<div id="bottomFooter">
        		<p>© 2014 GECCE. All Rights Reserved</p>
            	<div>
					<ul id="social" >
						<li><a href="#" id="finalTwitter">twitter</a></li>
						<li><a href="#" id="finalFacebook">facebook</a></li>
						<li><a href="#" id="finalFlickr">flickr</a></li>
						<li><a href="#" id="finalRss">rss</a></li>
					</ul>
					<ul id="legal">
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Terms &amp; Condition</a></li>
					</ul>
            	</div>
  			</div>
		</section>
  	</div>
</footer>

<!--Slides popup-->

<!--login slide starts-->
<!-- <div id="login-slide" class="slide-popup hide">
  <div class="overlay"></div>
  <div class="slide-content">
    <div class="slide-head">
      <h2>Login</h2>
    </div>
    <div class="slide-main">
      <form class="form-login" id="form-login" name="form-login">
        <input class="focus input" type="text" name="user_name" placeholder="Username">
        <input class="focus input" type="password" name="password" placeholder="password">
        <div class="checkbox clear">
          <input type="checkbox" name="remember_me"> Remember Me
        </div>
        <div class="clear">
        <a href="#/" class="forgot-pass-link">Forgot password?</a>
        </div>
        
        <input class="submit" type="submit" value="Submit">
      </form>
    </div>
  </div>
</div> -->
<!--login slide ends-->

<!--student login slide starts-->
<div id="student-login-slide" class=" hide">
  <div class="overlay"></div>
  <div class="slide-content">
    <div class="slide-head">
      <h2>Student Login</h2>
    </div>
    <div class="slide-main">
      <form class="form-student-login" action="under_construction" id="form-student-login" name="form-student-login">
        <input class="focus input" type="text" name="user_name" placeholder="Username">
        <input class="focus input" type="password" name="password" placeholder="password">
        <div class="checkbox clear">
          <input type="checkbox" name="remember_me"> Remember Me
        </div>
        <div class="clear">
        <a href="#/" class="forgot-pass-link">Forgot password?</a>
        </div>
        
        <input class="submit" type="submit" value="Submit">
      </form>
    </div>
  </div>
</div>
<!--student login slide ends-->

<!-- branch login slide starts-->
<div id="branch-login-slide" class=" hide">
  <div class="overlay"></div>
  <div class="slide-content">
    <div class="slide-head">
      <h2>Branch Login</h2>
    </div>
    <div class="slide-main">
      <form class="form-branch-login" action="under_construction" id="form-branch-login" name="form-branch-login">
        <input class="focus input" type="text" name="user_name" placeholder="Username">
        <input class="focus input" type="password" name="password" placeholder="password">
        <div class="checkbox clear">
          <input type="checkbox" name="remember_me"> Remember Me
        </div>
        <div class="clear">
        <a href="#/" class="forgot-pass-link">Forgot password?</a>
        </div>
        
        <input class="submit" type="submit" value="Submit">
      </form>
    </div>
  </div>
</div>
<!-- branch login slide ends-->


<!-- student verification starts -->
<div id="student-verification-slide" class="hide">
  <div class="overlay"></div>
  <div class="slide-content">
    <div class="slide-head">
      <h2>Student Verification : </h2>
    </div>
    <div class="slide-main">
      <form action="under_construction" class="form-student-verification" id="form-student-verification" name="form-student-verification">
        <input class="focus input" type="text" name="roll-no" placeholder="Please Enter Your Roll Number">
        <input class="submit" type="submit" value="Submit">
      </form>
    </div>
  </div>
</div>
<!-- student verification ends -->


<!-- query-form-slide starts -->
<div id="query-form-slide" class="hide">
  <div class="overlay"></div>
  <div class="slide-content">
    <div class="slide-head">
      <h2>Student Verification : </h2>
    </div>
    <div class="slide-main">
      <form action="under_construction" class="form-query-form" id="form-query-form" name="form-query-form">
        <input class="focus input" type="text" name="roll-no" placeholder="Please Enter Your Roll Number">
        <input class="focus input" type="text" name="roll-no" placeholder="Please Enter Your Full Address">
        <input class="focus input" type="text" name="roll-no" placeholder="Please Enter Your Mobile No.">
        <input class="focus input" type="text" name="roll-no" placeholder="Please Enter Your Email">
        <div class="focus input">
          <input type="radio" name="gender" value="male">Male</input>
          <input type="radio" name="gender" value="female">Female</input>
        </div>
        <select class="focus input">
          <option>(Please select a department)</option>
          <option>Admission</option>
          <option>Examination</option>
          <option>Result</option>
          <option>I-Card</option>
          <option>Other</option>
        </select>
        <input class="submit" type="submit" value="Submit">
      </form>
    </div>
  </div>
</div>
<!-- query-form-slide ends -->

<!-- date-sheet-slide starts -->
<div id="date-sheet-slide" class="hide">
  <div class="overlay"></div>
  <div class="slide-content">
    <div class="slide-head">
      <h2>Student Verification : </h2>
    </div>
    <div class="slide-main">
      <table border="0">

      <tbody><tr>
      <td style="width:300px; line-height:30px; color:#000000; font:bold;">Academic Year</td><td width="100">:</td><td style="width:200px; color:#000000; margin-left:500px; font:bold;" align="right">Show</td>
      </tr>

      <tr>
      <td style="width:300px; line-height:30px;">DATE SHEET IInd YearDecember 2015</td><td width="100">:</td><td style="width:200px;" align="right"><a href="https://docs.google.com/viewer?url=gecce.co.in/uploadeFiles/JAN 2015 II.pdf"> View</a></td>
      </tr>


      <tr>
      <td style="width:300px; line-height:30px;">DATE SHEET Ist YearDecember 2015</td><td width="100">:</td><td style="width:200px;" align="right"><a href="https://docs.google.com/viewer?url=gecce.co.in/uploadeFiles/2015 I.pdf"> View</a></td>
      </tr>

      </tbody></table>
    </div>
  </div>
</div>
<!-- date-sheet-slide ends -->

<!-- governing-body-slide starts -->
<div id="governing-body-slide" class="hide">
  <div class="overlay"></div>
  <div class="slide-content">
    <div class="slide-head">
      <h2>Student Verification : </h2>
    </div>
    <div class="slide-main">
      <table style="border-color:#eee; border:thick;" cellpadding="10" align="center">
  
  
  
        
      <tbody><tr style="width:630px; color:#000000;" bgcolor="#D3D1D2">
          <td style="width:30px; line-height:30px;">1</td>
          <td style="width:300px; line-height:30px;">Mr. P.S.Lodhi</td>
          <td style="width:300px; line-height:30px;">Secretary</td>
      </tr> 
        
        
            <tr bgcolor="#AEAEAE" style=" color:#747072; width:630px;">
          <td style="width:30px; line-height:30px;">2</td>
          <td style="width:300px; line-height:30px;">Mr. S.K Kaushal</td>
          <td style="width:300px; line-height:30px;">Depty Director</td>
      </tr> 
        
        
        
      <tr style="width:630px; color:#000000;" bgcolor="#D3D1D2">
          <td style="width:30px; line-height:30px;">3</td>
          <td style="width:300px; line-height:30px;">Mr. Saheb Singh</td>
          <td style="width:300px; line-height:30px;">Director</td>
      </tr> 
        
        
            <tr bgcolor="#AEAEAE" style=" color:#747072; width:630px;">
          <td style="width:30px; line-height:30px;">4</td>
          <td style="width:300px; line-height:30px;">Mrs. Nisha Verma</td>
          <td style="width:300px; line-height:30px;">Registrar</td>
      </tr> 
        
        
        
      <tr style="width:630px; color:#000000;" bgcolor="#D3D1D2">
          <td style="width:30px; line-height:30px;">5</td>
          <td style="width:300px; line-height:30px;">Kajal</td>
          <td style="width:300px; line-height:30px;">Examination Controller</td>
      </tr> 
        
        
            <tr bgcolor="#AEAEAE" style=" color:#747072; width:630px;">
          <td style="width:30px; line-height:30px;">6</td>
          <td style="width:300px; line-height:30px;">Dr. Santosh Kumar Sharma</td>
          <td style="width:300px; line-height:30px;">Co-ordinator(Aligarh)</td>
      </tr> 
        
        
        
      <tr style="width:630px; color:#000000;" bgcolor="#D3D1D2">
          <td style="width:30px; line-height:30px;">7</td>
          <td style="width:300px; line-height:30px;">Mr. Krishna Kant</td>
          <td style="width:300px; line-height:30px;">Zonal Co-ordinator Aligarh</td>
      </tr> 
        
        
            <tr bgcolor="#AEAEAE" style=" color:#747072; width:630px;">
          <td style="width:30px; line-height:30px;">8</td>
          <td style="width:300px; line-height:30px;">Mrs. Babita Kaushal</td>
          <td style="width:300px; line-height:30px;">Panel Controller(operations)</td>
      </tr> 
        
        
        
      <tr style="width:630px; color:#000000;" bgcolor="#D3D1D2">
          <td style="width:30px; line-height:30px;">9</td>
          <td style="width:300px; line-height:30px;">Mr. Umesh Gautam</td>
          <td style="width:300px; line-height:30px;">CEO(Chief Execitive Officer)</td>
      </tr> 
        
        
            <tr bgcolor="#AEAEAE" style=" color:#747072; width:630px;">
          <td style="width:30px; line-height:30px;">10</td>
          <td style="width:300px; line-height:30px;">Mr. Deepak Sapra</td>
          <td style="width:300px; line-height:30px;">National Co-ordinator</td>
      </tr> 
        
        
        
      <tr style="width:630px; color:#000000;" bgcolor="#D3D1D2">
          <td style="width:30px; line-height:30px;">11</td>
          <td style="width:300px; line-height:30px;">Mr.Munendra Kumar Mishra</td>
          <td style="width:300px; line-height:30px;">Zonal Co-Ordinator, u.p.east , Mob. 9452063599</td>
      </tr> 
        
        
            <tr bgcolor="#AEAEAE" style=" color:#747072; width:630px;">
          <td style="width:30px; line-height:30px;">12</td>
          <td style="width:300px; line-height:30px;">Mr. Neeraj Mishra</td>
          <td style="width:300px; line-height:30px;">Zonal Co-ordinator(NCR)</td>
      </tr> 
        
        
        
      <tr style="width:630px; color:#000000;" bgcolor="#D3D1D2">
          <td style="width:30px; line-height:30px;">13</td>
          <td style="width:300px; line-height:30px;">Mr. Basudev Jha</td>
          <td style="width:300px; line-height:30px;">Co-ordinator(Bihar State)</td>
      </tr> 
        
        
            <tr bgcolor="#AEAEAE" style=" color:#747072; width:630px;">
          <td style="width:30px; line-height:30px;">14</td>
          <td style="width:300px; line-height:30px;">Mrs. Neelam Verma</td>
          <td style="width:300px; line-height:30px;">Pannel Controller(Advisor)</td>
      </tr> 
        
        
        
      <tr style="width:630px; color:#000000;" bgcolor="#D3D1D2">
          <td style="width:30px; line-height:30px;">15</td>
          <td style="width:300px; line-height:30px;">Mr. Pawan Kumar</td>
          <td style="width:300px; line-height:30px;">State Co-Ordinator U.P</td>
      </tr> 
        
        
            <tr bgcolor="#AEAEAE" style=" color:#747072; width:630px;">
          <td style="width:30px; line-height:30px;">16</td>
          <td style="width:300px; line-height:30px;">Mr. ravish Kumar</td>
          <td style="width:300px; line-height:30px;">Legal Advisor</td>
      </tr> 
        
        
            <tr bgcolor="#AEAEAE" style=" color:#747072; width:630px;">
          <td style="width:30px; line-height:30px;">20</td>
          <td style="width:300px; line-height:30px;">Km.Prema Ekka</td>
          <td style="width:300px; line-height:30px;">Co-ordinator Chattishgarh &amp; Jharkhand State</td>
      </tr> 
        
        
        
      <tr style="width:630px; color:#000000;" bgcolor="#D3D1D2">
          <td style="width:30px; line-height:30px;">17</td>
          <td style="width:300px; line-height:30px;">R.S.Verma</td>
          <td style="width:300px; line-height:30px;">National Co-Ordinator ,   India</td>
      </tr> 
        
        
        
      <tr style="width:630px; color:#000000;" bgcolor="#D3D1D2">
          <td style="width:30px; line-height:30px;">19</td>
          <td style="width:300px; line-height:30px;">Ashok Meena</td>
          <td style="width:300px; line-height:30px;">Co-Ordinator,  West Bengal</td>
      </tr> 
        
        
        
      <tr style="width:630px; color:#000000;" bgcolor="#D3D1D2">
          <td style="width:30px; line-height:30px;">21</td>
          <td style="width:300px; line-height:30px;">Dr. Rajeev Tripathi</td>
          <td style="width:300px; line-height:30px;">Co-Ordinator ,  Jaunpur U.P</td>
      </tr> 
        
        
            <tr bgcolor="#AEAEAE" style=" color:#747072; width:630px;">
          <td style="width:30px; line-height:30px;">22</td>
          <td style="width:300px; line-height:30px;">Mr. Raj Kunwar</td>
          <td style="width:300px; line-height:30px;">Zonal Co-ordinator Varanasi</td>
      </tr> 
        
        
        
      <tr style="width:630px; color:#000000;" bgcolor="#D3D1D2">
          <td style="width:30px; line-height:30px;">23</td>
          <td style="width:300px; line-height:30px;">Mr. NITIN SRIVASTAVA</td>
          <td style="width:300px; line-height:30px;">Co-ordinator AZAMGARH ZONE U.P.</td>
      </tr> 
        
        
            <tr bgcolor="#AEAEAE" style=" color:#747072; width:630px;">
          <td style="width:30px; line-height:30px;">24</td>
          <td style="width:300px; line-height:30px;">MR MAYANK SINGH</td>
          <td style="width:300px; line-height:30px;">CO-ORDINATOR ALLAHABAD ZONE MOB. 9454160190</td>
      </tr> 
        
        
        
      <tr style="width:630px; color:#000000;" bgcolor="#D3D1D2">
          <td style="width:30px; line-height:30px;">25</td>
          <td style="width:300px; line-height:30px;">Mr Bhuvnesh Kumar Verma</td>
          <td style="width:300px; line-height:30px;">Co-Ordinator West India</td>
      </tr> 
        
        
            <tr bgcolor="#AEAEAE" style=" color:#747072; width:630px;">
          <td style="width:30px; line-height:30px;">26</td>
          <td style="width:300px; line-height:30px;">R.S. Raghav</td>
          <td style="width:300px; line-height:30px;">CEO (INDIA)</td>
      </tr> 
        
        
      </tbody></table>
    </div>
  </div>
</div>
<!-- governing-body-slide ends -->

<!-- download-slide starts -->
<div id="download-slide" class="hide">
  <div class="overlay"></div>
  <div class="slide-content">
    <div class="slide-head">
      <h2>Downloads Section </h2>
    </div>
    <div class="slide-main">
      <table border="0">

      <tbody><tr>
      <td style="width:300px; line-height:30px;">Admission Form</td><td width="100">:</td><td style="width:200px;" align="right">{{ link_to_route('under_construction', 'Download', [], ['target' => '_blank'] ); }}</td>
      </tr>
      <tr></tr>
      <tr>
      <td style="width:300px; line-height:30px;">Affilation Of Centre Form</td><td width="100">:</td><td style="width:200px;" align="right">{{ link_to_route('under_construction', 'Download', [], ['target' => '_blank'] ); }}</td>
      </tr>
      <tr></tr>
      <tr>
      <td style="width:300px; line-height:30px;">Declaration Form</td><td width="100">:</td><td style="width:200px;" align="right">{{ link_to_route('under_construction', 'Download', [], ['target' => '_blank'] ); }}</td>
      </tr>
      <tr></tr>
      <tr>
      <td style="width:300px; line-height:30px;">ISO Certificate</td><td width="100">:</td><td style="width:200px;" align="right">{{ link_to_route('under_construction', 'Download', [], ['target' => '_blank'] ); }}</td>
      </tr>
      <tr></tr>
      <tr>
      <td style="width:300px; line-height:30px;">Rules &amp; Regulation</td><td width="100">:</td><td style="width:200px;" align="right">{{ link_to_route('under_construction', 'Download', [], ['target' => '_blank'] ); }}</td>
      </tr>
      <tr>
      <td style="width:300px; line-height:30px;">Prospectus</td><td width="100">:</td><td style="width:200px;" align="right">{{ link_to_route('under_construction', 'Download', [], ['target' => '_blank'] ); }}</td>
      </tr>
      </tbody></table>
    </div>
  </div>
</div>
<!-- download-slide ends -->

<!-- download-slide starts -->
<div id="centers-slide" class="hide">
  <div class="overlay"></div>
  <div class="slide-content">
    <div class="slide-head">
      <h2>List Of Admision Centers </h2>
    </div>
    <div class="slide-main">
      <table border="1" style="border-color:#eee; border:thick;" cellpadding="10" align="center">
  
  
      <tbody><tr style="color:#FF9900;">
        <td><strong>Centre Code</strong></td>
        <td><strong>Centre Name</strong></td>
        <td><strong>Centre Manager</strong></td>
        <td><strong>Location</strong></td>
      </tr>
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">101</td>
        <td style="width:330px;">Shiwant Institute OF Management &amp; Technology</td>
        <td style="width:150px;">Dr. Santosh Sharma</td>
        <td style="width:50px;">Aligarh</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">102</td>
        <td style="width:330px;">Sangam Institute Of Education</td>
        <td style="width:150px;">Mrs. Neelam Verma</td>
        <td style="width:50px;">Rajnagar Ghaziabad</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">103</td>
        <td style="width:330px;">Manju Public Sr. Secondary College(Kasim Pur)</td>
        <td style="width:150px;">Mrs. Manju Verma</td>
        <td style="width:50px;">Kasimpur Aligarh            </td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">104</td>
        <td style="width:330px;">Shri Ram College Of Professinal Studies</td>
        <td style="width:150px;">Mr. Umesh Guatam</td>
        <td style="width:50px;">Ghaziabad</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">105</td>
        <td style="width:330px;">Apex Institute Of Technology &amp; Management</td>
        <td style="width:150px;">Mr. Pawan Kumar</td>
        <td style="width:50px;">Etah</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">106</td>
        <td style="width:330px;">Dastak Institute Of Education(Murad Nagar)</td>
        <td style="width:150px;">Mr. Rohit Kumar</td>
        <td style="width:50px;">Ghaziabad</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">107</td>
        <td style="width:330px;">Sardar Patel College Of Engineering &amp; Sciences</td>
        <td style="width:150px;">Rajesh Kumar</td>
        <td style="width:50px;">Rajender Nagar,bareilly</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">108</td>
        <td style="width:330px;">Jenius Tutorials</td>
        <td style="width:150px;">Lalit Kumar</td>
        <td style="width:50px;">West Model Town,Ghaziabad</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">109</td>
        <td style="width:330px;">New Organon Institute Of Education</td>
        <td style="width:150px;">Mrs. Pinki</td>
        <td style="width:50px;">Sik Road Ghaziabad</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">110</td>
        <td style="width:330px;">Elite Academy Computer Education</td>
        <td style="width:150px;">Mr. Mohar Singh</td>
        <td style="width:50px;">Pratap Vihar Khora Colony Ghaziabad</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">111</td>
        <td style="width:330px;">Gurukul Professional N Technicals Institute </td>
        <td style="width:150px;">Mr. Deepak Sapra</td>
        <td style="width:50px;">Raj Nagar Ghaziabad</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">112</td>
        <td style="width:330px;">Anmol Academy</td>
        <td style="width:150px;">Mr. Sanjay Jugran</td>
        <td style="width:50px;">Pratap Vihar Ghaziabad</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">113</td>
        <td style="width:330px;">Saraswati Institute Of Education</td>
        <td style="width:150px;">Mr. Satish Chand Sharma</td>
        <td style="width:50px;">Mangal Bihar Colony  Aligarh</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">114</td>
        <td style="width:330px;">Payal Institute of Education</td>
        <td style="width:150px;">Mr. Balveer Singh</td>
        <td style="width:50px;">Raipur, Sunamai Aligarh</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">115</td>
        <td style="width:330px;">Harish Chand Institute of Education(H.C.I.E)</td>
        <td style="width:150px;">Mr. Sunil Kumar</td>
        <td style="width:50px;">Garhi Guldhar Ghaziabad</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">116</td>
        <td style="width:330px;">Sangam Institute Of Mgt. &amp; Tech. Delhi</td>
        <td style="width:150px;">Roop Singh </td>
        <td style="width:50px;">Mayur Vihar Ph-III, Delhi</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">117</td>
        <td style="width:330px;">St. Bhoori Convent School</td>
        <td style="width:150px;">Mr. Raj Kumar</td>
        <td style="width:50px;">Bhim Nagar Bagu, Ghaziabad U.P</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">118</td>
        <td style="width:330px;">Sharan Education</td>
        <td style="width:150px;">Mrs.Pragya Jyoti Tiwari</td>
        <td style="width:50px;">Hanuman Ganj Sultanpur U.P</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">119</td>
        <td style="width:330px;">Shiv Mahesh Public School, Training Institute</td>
        <td style="width:150px;">Mr. Sankta Prasad</td>
        <td style="width:50px;">Amethi</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">120</td>
        <td style="width:330px;">Maa Saraswati Educational Institute </td>
        <td style="width:150px;">Mr.Santosh Kumar Gupta</td>
        <td style="width:50px;">Bhawanipur Kadipur Sultanpur (U.P)</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">121</td>
        <td style="width:330px;">Gyan Educational Institute jaunpur</td>
        <td style="width:150px;">Dr.Rajeev Tripathi</td>
        <td style="width:50px;">Sadbhawana Colony, Jaunpur</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">122</td>
        <td style="width:330px;">R.I.M.T</td>
        <td style="width:150px;">Mr. Neeraj Mishra</td>
        <td style="width:50px;">Bhangel, Noida (U.P)</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">123</td>
        <td style="width:330px;">N.J Teacher Training Centre</td>
        <td style="width:150px;">Mr. Narsing Jha</td>
        <td style="width:50px;">Sopaul (Bihar)</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">124</td>
        <td style="width:330px;">Saksham Institute of Mgt. &amp; Technology</td>
        <td style="width:150px;">Mr.Santosh Singh</td>
        <td style="width:50px;">Kishanpur, Aligarh U.P</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">125</td>
        <td style="width:330px;">Shivang Institute Of Education</td>
        <td style="width:150px;">Km. Sushma Singh</td>
        <td style="width:50px;">Dhanpatganj Sultanpur</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">126</td>
        <td style="width:330px;">Gmind institute </td>
        <td style="width:150px;">Mr. Kaushal Kumar Singh</td>
        <td style="width:50px;">Near vodafone office Tikoniapark Sultanpur</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">127</td>
        <td style="width:330px;">BICA</td>
        <td style="width:150px;">Mr. Pankaj Tekriwal</td>
        <td style="width:50px;">Bhelupur Varanasi U.P.</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">128</td>
        <td style="width:330px;">Lucknow Computer Institute</td>
        <td style="width:150px;">Mr. Sanjay Gupta</td>
        <td style="width:50px;">O.P.Singh niwas gali, near AAJ press Badhaiyabeer Sultanpur U.P.</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">130</td>
        <td style="width:330px;">Dev Raj Institute of Mgt. &amp; Technology (DIMT)</td>
        <td style="width:150px;">Mr. Ganesh Kumar</td>
        <td style="width:50px;">Patel Nagar-III, Ghaziabad U.P</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">131</td>
        <td style="width:330px;">Next Stepladder of Professional Studies (NSPS)</td>
        <td style="width:150px;">Mr. Dheeraj Chopra</td>
        <td style="width:50px;">Abhay Khand-III, Indirapuram Ghaziabad</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">132</td>
        <td style="width:330px;">Sun Academy</td>
        <td style="width:150px;">Mr. Sandeep Mishra</td>
        <td style="width:50px;">Near Dainik Jagaran Office PWD road sultanpur U.P.</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">133</td>
        <td style="width:330px;">Akhilesh memorial education</td>
        <td style="width:150px;">Mr. Vinay pandey</td>
        <td style="width:50px;">Near children academy vivek nagar sultanpur u.p</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">151</td>
        <td style="width:330px;">SHREYAM COMPUTER AND EDUCATIONAL INSTITUTE KADIPUR SULTANPUR</td>
        <td style="width:150px;">Mr. SHIVAM TIWARI</td>
        <td style="width:50px;">NIRALA NAGAR IN FRONT OF BHARAT PETROLIUM KADIPUR SULTANPUR PIN 228145</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">135</td>
        <td style="width:330px;">Apollo Institute IT Shahganj  Jaunpur U.P</td>
        <td style="width:150px;">Mr. Raj Kumar Gupta</td>
        <td style="width:50px;">Jaunpur, Uttar Pradesh</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">136</td>
        <td style="width:330px;">B.M Institute</td>
        <td style="width:150px;">Mr. Lal chandra pandey</td>
        <td style="width:50px;">Chanda bazar sultanpur (U.p)</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">00137</td>
        <td style="width:330px;">Bright Computer Institute</td>
        <td style="width:150px;">Mr. Daya sankar Shukla</td>
        <td style="width:50px;">In Front of Rani ganesh Kunwar mahavidyalay jamo Amethi (U.P.)</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">148</td>
        <td style="width:330px;">NEW EXCELLENT INSTITUTE OF MANAGEMENT &amp; TECHNOLOGY</td>
        <td style="width:150px;">MR. SHASHI KUMAR</td>
        <td style="width:50px;">BRANCH 2- 34 BN PAC CAMPUS SOUTH GATE BHULLANPUR VARANASI U.P. 221108</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">149</td>
        <td style="width:330px;">ARZOO COMPUTER</td>
        <td style="width:150px;">MR KADIR AHMED</td>
        <td style="width:50px;">VILLEGE AND POST BHANPURI COLONEY LAKHIMPUR KHERI U.P.</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">0150</td>
        <td style="width:330px;">Dr. Vijay Kumar Vijayvargiya</td>
        <td style="width:150px;">Career Line Institute of Education</td>
        <td style="width:50px;">Udaipur Rajasthan</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">139</td>
        <td style="width:330px;">Lord Budha Vocational Training Center</td>
        <td style="width:150px;">Mrs. Vijay Laxmi</td>
        <td style="width:50px;">Annapurna Packers , Sikhera Road Modinagar</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">140</td>
        <td style="width:330px;"> KUSUM INSTITUTE OF INFORMATION &amp; TECHNOLOGY SULTANPUR</td>
        <td style="width:150px;">MRS  KUSUM SINGH</td>
        <td style="width:50px;">III FLOOR REHAN COMPLEX AT  DURGA MARKET NEAR OVER BRIDGE SULTANPUR U.P. 228001 Mob. 8090805577</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">141</td>
        <td style="width:330px;">PARAM BHARAT TRAINING CENTER</td>
        <td style="width:150px;">VIJAY KUMAR</td>
        <td style="width:50px;">NEAR ASHA HOSPITAL MUIR ROAD RAJAPUR ALLAHABAD U.P. MOB. 7786845091</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">138</td>
        <td style="width:330px;">GLOBAL INSTITUTE OF INFORMATION &amp; TECHNOLOGY (GIIT) MOB - 9651307257</td>
        <td style="width:150px;">MR SANDEEP SRIVASTAVA</td>
        <td style="width:50px;">793 civil line 1, diwani chauraha sitakund sultanpur</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">143</td>
        <td style="width:330px;">KRITI KARTIKEY EDUCATION</td>
        <td style="width:150px;">DR. PRANVIJAY CHAUBEY</td>
        <td style="width:50px;">AVADH SHANTI COMPLEX LOHRAMAU ROAD OM NAGAR SULTANPUR U.P. 228001 </td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">144</td>
        <td style="width:330px;">Mother Teresa Nursery Teacher Training Center</td>
        <td style="width:150px;">Mrs. Seema </td>
        <td style="width:50px;">Universal Pub. School Aurangabad Bulandshahr</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">145</td>
        <td style="width:330px;">SEWS ACADEMY</td>
        <td style="width:150px;">MR SYED ZAHOOR SABIR</td>
        <td style="width:50px;">LIG 3/745 SEC H JANKIPURAM LUCKNOW - 226021</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">0146</td>
        <td style="width:330px;">Jai Maa Sharda Education Centre</td>
        <td style="width:150px;">Mr. Dinesh Chandra Mishra</td>
        <td style="width:50px;">Ahda Sultanpur</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">0147</td>
        <td style="width:330px;">LIVES INSTITUTE PALAMPUR </td>
        <td style="width:150px;">MR PRAVEEN KUMAR</td>
        <td style="width:50px;">KANGRA HIMACHAL PRADESH</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">0148</td>
        <td style="width:330px;">INDIRA EDUCATION CENTER </td>
        <td style="width:150px;">MR RAMESH GUPTA</td>
        <td style="width:50px;">SURAPUR SULTANPUR U.P</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">153</td>
        <td style="width:330px;">Janhit Educational Institute . Azamgarh.</td>
        <td style="width:150px;">Nitin Srivastav</td>
        <td style="width:50px;">azamgarh</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">152</td>
        <td style="width:330px;">renu mahila silai prashikshan kendra pakhrauli</td>
        <td style="width:150px;">renu baranwal</td>
        <td style="width:50px;">pakhrauli district sultanpur u.p.</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">154</td>
        <td style="width:330px;">Shri Ram Saran Singh Institute Of Mgt. &amp; Technology</td>
        <td style="width:150px;">Smt. Sita Yadav</td>
        <td style="width:50px;">Shahjahanpur (U.P)</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">155</td>
        <td style="width:330px;">Durga Group Of Education</td>
        <td style="width:150px;">Mr. Vinay Kumar</td>
        <td style="width:50px;">Malka Gang Delhi</td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">156</td>
        <td style="width:330px;">G-NEXT INSTITUTE OF  EDUCATIONS</td>
        <td style="width:150px;">Mr. Shashank sekhar dwivedi</td>
        <td style="width:50px;"> Tirahut Bazar – sultanpur </td>
    </tr> 
      
      
        <tr style="color:#CC6600; width:630px; line-height:30px;" bgcolor="#AEAEAE">
        <td style="width:100px;">0158</td>
        <td style="width:330px;">Pratibha NTT Training Center Dibai</td>
        <td style="width:150px;">Mrs. Reena Raghav</td>
        <td style="width:50px;">Dibai Bulandshahr Uttar Pradesh</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">0157</td>
        <td style="width:330px;">RAGHUVANSH MANI COMPUTERS</td>
        <td style="width:150px;">MR. RAKESH SINGH RAGHUVANSHI</td>
        <td style="width:50px;">SAMPURNA NAGAR, LAKHIMPUR KHERI U.P</td>
    </tr> 
      
      
      
    <tr style="width:630px; line-height:30px;" bgcolor="#D3D1D2">
        <td style="width:100px;">0159</td>
        <td style="width:330px;">EDUSAGE EDUCATIONAL SERVICES</td>
        <td style="width:150px;">MR. VASI &amp; MR. KAUSHAL SINGH</td>
        <td style="width:50px;">NOIDA U.P</td>
    </tr> 
      
      
    </tbody></table>
    </div>
  </div>
</div>
<!-- centers-slide ends -->

{{HTML::script('js/jquery.min.js')}}
{{HTML::script('js/jquery.flexslider-min.js')}}
{{HTML::script('js/lightbox.js')}}
{{HTML::script('js/scripts.js')}}
{{HTML::script('js/navigation.js')}}

@yield('footer-scripts')

</body>
</html>