@extends('frontend/layout/master')
  @section('content')
        <div id="contentBk" class="clearfix">
            <div id="content">
                <div class="topImg clearfix">
                    {{HTML::image('images/headers/header_1.jpg', 'About Us')}}
                    <p>Syllabus Description</p>
                </div>
                <div class="wrapper">
                    <div class="column c-33 clearfix">
                        <div class="box">
                            <h4>NTT : Nursery Teacher Training- 1 years</h4>
                            <div class="boxInfo">
                                <a title="Examinator center" class="gHover" href="{{public_path() . '/images/events/e2_large.jpg'}}" rel="lightbox">
                                {{HTML::image('images/events/e2_large_gallery.jpg', '', array("class"=>"fwidth"))}}
                                </a>
                                <p><table>
                    
                                    <tbody><tr>
                                        <td style="width:100px;">Theory</td>
                                        <td style="width:440px;">Structure</td>
                                        <td style="width:100px;">(700 Marks)</td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px;">1st Paper</td>
                                        <td style="width:440px;">Child Psychology</td>
                                        <td style="width:100px;">100</td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px;">2nd Paper</td>
                                        <td style="width:440px;">Child Care &amp; Health</td>
                                        <td style="width:100px;">100</td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px;">3rd Paper</td>
                                        <td style="width:440px;">Sociology &amp; Guidance</td>
                                        <td style="width:100px;">100</td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px;">4th Paper</td>
                                        <td style="width:440px;">School Organization</td>
                                        <td style="width:100px;">100</td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px;">5th Paper</td>
                                        <td style="width:440px;">Principles of Education</td>
                                        <td style="width:100px;">100</td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px;">6th Paper</td>
                                        <td style="width:440px;">Educational Psychology</td>
                                        <td style="width:100px;">100</td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px;">7th Paper</td>
                                        <td style="width:440px;">Modern Methods of Teaching</td>
                                        <td style="width:100px;">100</td>
                                    </tr>
                                </tbody></table>
                            </p>
                            </div>
                        </div>
                    </div>

                    <div class="column c-33 clearfix">
                        <div class="box">
                            <h4>NTT : Nursery Teacher Training- 2 years</h4>
                            <div class="boxInfo">
                                <a title="Examinator center" class="gHover" href="{{public_path() . '/images/events/e2_large.jpg'}}" rel="lightbox">
                                {{HTML::image('images/events/e2_large_gallery.jpg', '', array("class"=>"fwidth"))}}
                                </a>
                                <p><table>
                
                    <tbody><tr>
                        <td style="width:100px;">Theory</td>
                        <td style="width:440px;">Structure</td>
                        <td style="width:100px;">(700 Marks)</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">1st Paper</td>
                        <td style="width:440px;">Methods of Teaching</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">2nd Paper</td>
                        <td style="width:440px;">Working with Parents &amp; Community</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">3rd Paper</td>
                        <td style="width:440px;">Pre-Primary(Nursery) Education</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">4th Paper</td>
                        <td style="width:440px;">Teacher Education</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">5th Paper</td>
                        <td style="width:440px;">Computer Education</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">6th Paper</td>
                        <td style="width:440px;">PRACTICAL WORK</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">7th Paper</td>
                        <td style="width:440px;">VIVA INTERNSHIP &amp; TRAINING</td>
                        <td style="width:100px;">100</td>
                    </tr>
                </tbody></table></p>
                            </div>
                        </div>
                    </div>

                    <div class="column c-33 clearfix">
                        <div class="box">
                            <h4>PTT : Primary Teacher Training- 1 years</h4>
                            <div class="boxInfo">
                                <a title="Examinator center" class="gHover" href="{{public_path() . '/images/events/e2_large.jpg'}}" rel="lightbox">
                                {{HTML::image('images/events/e2_large_gallery.jpg', '', array("class"=>"fwidth"))}}
                                </a>
                                <p>
                                    <table>
                    
                    <tbody><tr>
                        <td style="width:100px;">Theory</td>
                        <td style="width:440px;">Structure(Compulsary Subjects)</td>
                        <td style="width:100px;">(500 Marks)</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">1st Paper</td>
                        <td style="width:440px;">Curriculum &amp; Instruction</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">2nd Paper</td>
                        <td style="width:440px;">Psychogy of learning &amp; Development</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">3rd Paper</td>
                        <td style="width:440px;">Education &amp; Evalution</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">4th Paper</td>
                        <td style="width:440px;">Education &amp; Society</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">5th Paper</td>
                        <td style="width:440px;">Teacher &amp; School</td>
                        <td style="width:100px;">100</td>
                    </tr>                   
                </tbody></table>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="column c-33 clearfix">
                        <div class="box">
                            <h4>PTT : Primary Teacher Training- 2 years</h4>
                            <div class="boxInfo">
                                <a title="Examinator center" class="gHover" href="{{public_path() . '/images/events/e2_large.jpg'}}" rel="lightbox">
                                {{HTML::image('images/events/e2_large_gallery.jpg', '', array("class"=>"fwidth"))}}
                                </a>
                                <p>
                                    <table>
                    
                    <tbody><tr>
                        <td style="width:100px;">Theory</td>
                        <td style="width:440px;">Structure</td>
                        <td style="width:100px;">(700 Marks)</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">1st Paper</td>
                        <td style="width:440px;">Education Technology</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">2nd Paper</td>
                        <td style="width:440px;">Computer in Education</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">3rd Paper</td>
                        <td style="width:440px;">Guidance &amp; Counseling</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">4th Paper</td>
                        <td style="width:440px;">HIV &amp; AIDS Education</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">5th Paper</td>
                        <td style="width:440px;">Addesecence &amp; Family Education</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">6th Paper</td>
                        <td style="width:440px;">Practical Work</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">7th Paper</td>
                        <td style="width:440px;">Viva-Internship &amp; Traning</td>
                        <td style="width:100px;">100</td>
                    </tr>                   
                </tbody></table>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="column c-33 clearfix">
                        <div class="box">
                            <h4>DCA : Diploma in Computer Application</h4>
                            <div class="boxInfo">
                                <a title="Examinator center" class="gHover" href="{{public_path() . '/images/events/e2_large.jpg'}}" rel="lightbox">
                                {{HTML::image('images/events/e2_large_gallery.jpg', '', array("class"=>"fwidth"))}}
                                </a>
                                <p>
                                    <table>
                    
                    <tbody><tr>
                        <td style="width:100px;">Theory</td>
                        <td style="width:440px;">Structure</td>
                        <td style="width:100px;">(900 Marks)</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">1st Paper</td>
                        <td style="width:440px;">Fundamental of Computer &amp; C</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">2nd Paper</td>
                        <td style="width:440px;">Operating System</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">3rd Paper</td>
                        <td style="width:440px;">Basic of Internet</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">4th Paper</td>
                        <td style="width:440px;">Object Oriented Programming Using C++</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">5th Paper</td>
                        <td style="width:440px;">Database Management System</td>
                        <td style="width:100px;">100</td>
                    </tr>   
                    <tr>
                        <td style="width:100px;">6th Paper</td>
                        <td style="width:440px;">Computer Architecture</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">7th Paper</td>
                        <td style="width:440px;">MS Office</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">8th Paper</td>
                        <td style="width:440px;">Project Work</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">9th Paper</td>
                        <td style="width:440px;">Viva - Internship &amp; Trainings</td>
                        <td style="width:100px;">100</td>
                    </tr>                   
                </tbody></table>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="column c-33 clearfix">
                        <div class="box">
                            <h4>CTT : Computer Teacher Training</h4>
                            <div class="boxInfo">
                                <a title="Examinator center" class="gHover" href="{{public_path() . '/images/events/e2_large.jpg'}}" rel="lightbox">
                                {{HTML::image('images/events/e2_large_gallery.jpg', '', array("class"=>"fwidth"))}}
                                </a>
                                <p>
                                    <table>
                    
                    <tbody><tr>
                        <td style="width:100px;">Theory</td>
                        <td style="width:440px;">Structure</td>
                        <td style="width:100px;">(500 Marks)</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">1st Paper</td>
                        <td style="width:440px;">Fundamentals Of Computer And Office Automation</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">2nd Paper</td>
                        <td style="width:440px;">Internet Programming And Web Designing</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">3rd Paper</td>
                        <td style="width:440px;">Principles Of Teaching Technology</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">4th Paper</td>
                        <td style="width:440px;">PROGRAMMING IN C++</td>
                        <td style="width:100px;">100</td>
                    </tr>
                    <tr>
                        <td style="width:100px;">5th Paper</td>
                        <td style="width:440px;">Communication Skills In English</td>
                        <td style="width:100px;">100</td>
                    </tr>                   
                </tbody></table>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="column c-33 clearfix">
                        <div class="box">
                            <h4>College library</h4>
                            <div class="boxInfo">
                                <a title="College library" class="gHover" href="{{public_path() . '/images/events/e1_large.jpg'}}" rel="lightbox">{{HTML::image('images/events/e1_large_gallery.jpg', '', array("class"=>"fwidth"))}}
                                <p>Lorem ipsum doloreiusmod tempor dolorei doloreiusmod tempor usmod tempordolor eiusmod tempor</p>
                            </div>
                        </div>
                    </div>
                    <div class="column c-33 clearfix">
                        <div class="box">
                            <h4>Examinator center</h4>
                            <div class="boxInfo">
                                <a title="Examinator center" class="gHover" href="{{public_path() . '/images/events/e2_large.jpg'}}" rel="lightbox">
                                {{HTML::image('images/events/e2_large_gallery.jpg', '', array("class"=>"fwidth"))}}
                                </a>
                                <p>Lorem ipsum doloreiusmod tempor dolorei doloreiusmod tempor usmod tempordolor eiusmod tempor</p>
                            </div>
                        </div>
                    </div>
                    <div class="column c-33 clearfix">
                        <div class="box">
                            <h4>Classroom</h4>
                            <div class="boxInfo">
                                <a title="Classroom" class="gHover" href="{{public_path() . '/images/events/e3_large.jpg'}}" rel="lightbox">
                                {{HTML::image('images/events/e3_large_gallery.jpg', '', array("class"=>"fwidth"))}}</a>
                                <p>Lorem ipsum doloreiusmod tempor dolorei doloreiusmod tempor usmod tempordolor eiusmod tempor</p>
                            </div>
                        </div>
                    </div>

                    <div class="clear"></div>

                      <div class="column c-33 clearfix">
                        <div class="box">
                            <h4>College library</h4>
                            <div class="boxInfo">
                                <a title="College library" class="gHover" href="{{public_path() . '/images/events/e4_large.jpg'}}" rel="lightbox">
                                {{HTML::image('images/events/e4_large_gallery.jpg', '', array("class"=>"fwidth"))}}</a>
                                <p>Lorem ipsum doloreiusmod tempor dolorei doloreiusmod tempor usmod tempordolor eiusmod tempor</p>
                            </div>
                        </div>
                    </div>
                    <div class="column c-33 clearfix">
                        <div class="box">
                            <h4>Examinator center</h4>
                            <div class="boxInfo professor">
                                <a title="Examinator center" class="gHover" href="{{public_path() . '/images/events/e5_large.jpg'}}" rel="lightbox">
                                {{HTML::image('images/events/e5_large_gallery.jpg', '', array("class"=>"fwidth"))}}></a>
                                <p>Lorem ipsum doloreiusmod tempor dolorei doloreiusmod tempor usmod tempordolor eiusmod tempor</p>
                            </div>
                        </div>
                    </div>
                    <div class="column c-33 clearfix">
                        <div class="box">
                            <h4>Classroom</h4>
                            <div class="boxInfo">
                                <a title="Classroom" class="gHover" href="{{public_path() . '/images/events/e6_large.jpg'}}" rel="lightbox">
                                {{HTML::image('images/events/e7_large_gallery.png', '', array("class"=>"fwidth"))}}</a>
                                <p>Lorem ipsum doloreiusmod tempor dolorei doloreiusmod tempor usmod tempordolor eiusmod tempor</p>
                            </div>
                        </div>
                    </div> -->

                    <div class="column c-100 clearfix">
                        <ul class="pgNr">
                            <li><a href="under_construction"> 1</a></li>
                            <li><a href="under_construction"> 2</a></li>
                            <li><a href="under_construction"> 3</a></li>
                            <li><a href="under_construction"> 4</a></li>
                            <li><a href="under_construction"> ></a></li>
                        </ul>
                    </div>

                </div> 

            </div>
        </div>
    @stop