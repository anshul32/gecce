<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array( 'as' => 'home' , function()
{
	return View::make('frontend.index');
}));

Route::get('/about', array( 'as' => 'about', function()
{
	return View::make('frontend.about');
}));

Route::get('/course', array( 'as' => 'course', function()
{
	return View::make('frontend.course');
}));

Route::get('/student_zone', array( 'as' => 'student_zone', function()
{
	return View::make('frontend.student-zone');
}));

Route::get('/course-detail', array( 'as' => 'course-detail', function()
{
	return View::make('frontend.course-detail');
}));

Route::get('/contact', array('as' => 'contact', function()
{
	return View::make('frontend.contact');
}));

Route::get('/syllabus', array('as' => 'syllabus', function()
{
	return View::make('frontend.syllabus');
}));

Route::get('/download', array('as' => 'download', function()
{
	return View::make('frontend.download');
}));

Route::get('/under_construction', array('as' => 'under_construction', function()
{
	return View::make('frontend.error_screen');
}));

Route::resource('users', 'UsersController');
Route::get('register', ['uses' => 'UsersController@create']);
Route::get('login', ['uses' => 'SessionsController@create', 'as' => 'login']);
Route::post('login', ['uses' => 'SessionsController@store', 'as' => 'login']);
Route::get('logout', ['uses' => 'SessionsController@destroy', 'as' => 'logout']);

Route::get('/admin/dashboard', function(){
	return View::make('backend/dashboard');
});

Route::resource('branchs', 'BranchController');
Route::resource('courses', 'CourseController');
Route::resource('syllabus', 'SyllabusController');
Route::resource('subjects', 'SubjectController');
Route::resource('governings', 'GoverningController');
Route::resource('downloads', 'DownloadController');
Route::resource('students', 'StudentController');