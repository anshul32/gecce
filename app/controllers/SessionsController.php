<?php

class SessionsController extends \BaseController {


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.users.login');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$attrs = Input::only('email', 'password');
		$remember = Input::get('remember');

		if(Auth::attempt($attrs, $remember))
		{
			$u = Auth::user();
			$urlToapp = route('app.index');
			return Redirect::intended($urlToapp);
		}

		return Redirect::back()->withMessage('Wrong credentials!')->withInput();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();
		return Redirect::route('login');
	}


}
