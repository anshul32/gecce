<?php

class SyllabusController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$syllabus = Syllabus::get()->toArray();
		return View::make('backend/syllabus/index', compact('syllabus'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$courses = Course::lists('title', 'id');
		// dd($courses);
		return View::make('backend/syllabus/create', compact('courses'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$attrs = Input::all();
		$syllabus =  $this->createNew('Syllabus', $attrs);
		// dd($syllabus);
		DB::table('course_syllabus')->insert(['course_id' => $attrs['course_id'], 'syllabus_id' =>  $syllabus['id'] ]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// dd($id);
		// $syllabus = Syllabus::whereId($id)->with(['course'])->get()->toArray();
		// $syllabus = Syllabus::where('id', $id)->with(['course'])->lists('id', 'title');
		// $syllabus = Syllabus::findOrFail($id)->with(['course'])->first()->toArray();
		// dd($syllabus);
		$courses = Course::lists('title', 'id');
		return View::make('backend/syllabus/edit', compact('syllabus', 'courses'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
