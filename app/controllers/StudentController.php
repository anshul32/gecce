<?php

class StudentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('backend/student/index');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$course = Course::lists('title', 'id');

		$branch = User::where('type', 2)->lists('email', 'id');

		return View::make('backend/student/create', compact('course', 'branch'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		// first create student object in student table
		$attrs = Input::all();
		return $student =  $this->createNew('Student', $attrs);
		// DB::table('course_syllabus')->insert(['course_id' => $attrs['course_id'], 'syllabus_id' =>  $syllabus['id'] ]);

		// and save as a user in users table. 
		// so that student will be able to login.
		$attrs['manager_name'] = '_';
		$student_as_user =  $this->createNew('User', $attrs);
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
