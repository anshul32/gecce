<?php

class ApiController extends BaseController {

 /**
  * Setup the layout used by the controller.
  *
  * @return void
  */

    static function save($table, $data, $extraFields = false, $getLastInsertedId = false)
    {

        if($extraFields)
        {
         foreach ($extraFields as $field) {
          unset($data[$field]);
         }
        }
        if($getLastInsertedId)
        {
                $instertedId = $table::insertGetId($data);
         return $instertedId;
        }
        
        $insterted = $table::insert($data);
        return $insterted;
    }

    static function update($table, $data, $primeryField, $fieldId, $extraFields = false)
    {
        if($extraFields)
        {
         foreach ($extraFields as $field) {
          unset($data[$field]);
         }
        }
        
        if($table::where($primeryField, $fieldId)->update($data))
        {
            return true;
        }
        
        return false;
        //$queries = DB::getQueryLog();
    }

}