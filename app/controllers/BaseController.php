<?php
use Illuminate\Http\Response;
class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	protected function createNew($entity, $attrs)
	{
		$e = new $entity($attrs);

		if(!$e->save())
		{
			return Redirect::back()->withErrors($e->getErrors())->withInput();
		} 
		return $e;
		// return R
	}

}
