<?php

class BranchController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$branchs = User::where('type', 2)->get()->toArray();
		
		return View::make('backend/branch/index', compact('branchs'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend/branch/create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$attrs = Input::all();
		// $branch = User::where('email', $attrs['email']);

		$attrs['type'] = 2; // branch
		if(array_get($attrs, 'password'))
		{
			$password = &$attrs['password'];
			$password = Hash::make($password);
		}
		
		return $this->createNew('User', $attrs);
		// return Redirect::back()->with('message','Operation Successful !');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$branch = User::findOrFail($id)->toArray();
		// dd($branch);
		return View::make('backend/branch/edit', compact('branch'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$branch = User::findOrFail($id);
		$branch->fill(Input::all());
		$branch->save();

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
