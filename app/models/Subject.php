<?php

// use app\models\Course;

class Subject extends ApiModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'subjects';

	public static $rules = [
		'title'			=> 'required',
		'type'			=> 'required',
		'marks'			=> 'required',
		// 'syllabus_id'	=> 'required'
	];

	protected $fillable = [ 'title', 'type', 'marks']; // we wont want to include syllabus_id in fillable.

	public function syllabus(){
		return $this->belongsTo('Subject', 'id');
	}
}


