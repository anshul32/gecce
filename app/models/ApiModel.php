<?php

class ApiModel extends \Eloquent{
	
	protected $errors;


	/**
	 * Laravel automatically calls the static boot functions on instantiation
	 */
	public static function boot()
	{
		parent::boot();

		static::creating(function($model)
		{
			return $model->validate();
		});
	}

	public function validate()
	{

		$validation = \Validator::make($this->getAttributes(), static::$rules);

		if($validation->fails())
		{
			$this->errors = $validation->messages();

			return false; // model will not be saved
		}

		return true; // model will be saved
	}

	public function getErrors()
	{
		return $this->errors;
	}
}