<?php

// use app\models\Course;

class Student extends ApiModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'students';

	public static $rules = [
		"name"				=> "required",
		"dob"				=> "required",
		"place_of_birth"	=> "required",
		"father_name"		=> "required",
		"mother_name"		=> "required",
		"address"			=> "required",
		"email"				=> "required",
		"contact"			=> "required",
		"language"			=> "required",
		"gender"			=> "required",
		"nationality"		=> "required",
		"course_id"			=> "required",
		"qualification"		=> "required",
		"center_id"			=> "required",
		"about"				=> "required",
	];

	protected $fillable = ["name", "dob", "place_of_birth", "father_name", "mother_name", "address", "email", "contact", "language", "gender", "nationality", "course_id", "qualification", "center_id", "about" ];

}


