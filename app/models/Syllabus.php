<?php

use app\models\Course;

class Syllabus extends ApiModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'syllabus';

	public static $rules = [
		// 'course_id'		=> 'required',
		'title'			=> 'required'
	];

	protected $fillable = ['title'];

	public function course(){
		return $this->belongsTo('Course', 'id');
	}

	public function subjects(){
		return $this->hasMany('Subject');
	}
}


