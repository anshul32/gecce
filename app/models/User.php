<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends ApiModel implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	public static $rules = [
		'manager_name' 	=> 'required',
		'email'			=> 'required|email',
		'password'		=> 'required', 
		'type'			=> 'required', 
		'contact'		=> 'required|numeric', 
		'address'		=> 'required'
	];

	protected $fillable = ['manager_name', 'email', 'password', 'type', 'contact', 'address'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

}


