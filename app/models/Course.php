<?php

class Course extends ApiModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'courses';

	public static $rules = [
		'branch_id'			=> 'required',
		'title'				=> 'required', 
		'description'		=> 'required'
	];

	protected $fillable = ['branch_id', 'title', 'description'];

	public function syllabus(){
		return $this->hasMany('Syllabus');
	}
}


