<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('manager_name');
			$table->string('email')->unique();
			$table->string('password');
			$table->enum('type', [ 1, 2, 3 ]); // 1 for admin. 2 for branch. 3 for student.
			$table->bigInteger('contact')->unsigned();
			$table->text('address');			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
