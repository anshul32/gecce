<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('students', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->date('dob');
			$table->string('place_of_birth');
			$table->string('father_name');
			$table->string('mother_name');
			$table->text('address');
			$table->string('email')->unique();
			$table->integer('contact')->unsigned();
			$table->text('language');
			$table->text('gender');
			$table->string('nationality');
			$table->string('course_id');
			$table->string('qualification');
			$table->string('center_id');
			$table->text('about');			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('students');
	}

}
