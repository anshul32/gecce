$('#student-login').on('click', function(e) {
    e.preventDefault();
    $('#student-login-slide').addClass('slide-popup');
});

$('#branch-login').on('click', function(e) {
    e.preventDefault();
    $('#branch-login-slide').addClass('slide-popup');
});

$('#student-verfication').on('click', function(e) {
    e.preventDefault();
    $('#student-verification-slide').addClass('slide-popup');
});

$('#query-form').on('click', function(e) {
    e.preventDefault();
    $('#query-form-slide').addClass('slide-popup');
});

$('#date-sheet').on('click', function(e) {
    e.preventDefault();
    $('#date-sheet-slide').addClass('slide-popup');
});

$('#governing-body').on('click', function(e) {
    e.preventDefault();
    $('#governing-body-slide').addClass('slide-popup');
});

$('#download').on('click', function(e) {
    e.preventDefault();
    $('#download-slide').addClass('slide-popup');
});

$('#centers').on('click', function(e) {
    e.preventDefault();
    $('#centers-slide').addClass('slide-popup');
});

